<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->name,
        'email' => str_random('3'),
        'username' => $faker->userName,
        'password' => app('hash')->make('secret'),
        'address' => $faker->address,
        'birthday' => '1994-01-02',
        'status' => 1,
        'gender' => array_rand(['male','female','other']),
        'mobile' => $faker->phoneNumber,

    ];
});
