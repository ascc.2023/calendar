<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_event_id')->nullable();
//            $table->foreign('parent_event_id')->references('id')->on('events');
            $table->string('title');
            $table->text("description");
            $table->dateTime('start_date');
            $table->datetime('end_date');
            $table->tinyInteger('is_full_day_event');
            $table->datetime('recurring_end_date')->nullable();
            $table->tinyInteger('is_recurring');
            $table->string('created_by');
            $table->string('organiser');
            $table->tinyInteger('has_reminder');
            $table->text('location');
            $table->text('guests');
            $table->enum('rsvp',['yes','no','maybe'])->nullable();
            $table->text('note');
            $table->string('event_permission')->nullable();
            $table->enum('visibility',['free','busy']);
            $table->enum('privacy',['private','public','default']);
            $table->tinyInteger('status');
            $table->integer('duration')->nullable();
            $table->text('rrule');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
