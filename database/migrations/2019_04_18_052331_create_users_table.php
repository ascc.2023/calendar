<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name')->default('');
//            $table->string('username');
            $table->string('password')->default('');
            $table->string('email')->unique();
            $table->enum('gender',['male','female','other'])->nullable();
            $table->string('mobile')->default('');
            $table->date('birthday')->nullable();
            $table->text('address');
            $table->tinyInteger('status');
            $table->string('avatar')->default('');
            $table->text('social_login');//store social login info like  10101:facebook,110abasc:google
            $table->rememberToken();
            $table->timestamps();
            $table->engine = "InnoDB";

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
