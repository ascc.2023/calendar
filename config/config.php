<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/19/19
 * Time: 3:20 PM
 */

return [
    "age_limit" => 18,
    'grant_type' => '',
    'client_id' => '',
    'client_name' => '',
    'user_id' => '',
    'providers' => [
        'gerp' => 'https://auth.geniuserp.dev.genisys.cf'
    ],
];