FROM composer as builder

WORKDIR /var/www
COPY . .

RUN composer install --no-dev --no-interaction -o

FROM php:7.2-fpm

RUN apt-get update && apt-get install libpng-dev -y
RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql && docker-php-ext-install gd && docker-php-ext-enable gd

WORKDIR /var/www
COPY --from=builder /var/www .

