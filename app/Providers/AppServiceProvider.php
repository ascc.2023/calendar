<?php

namespace App\Providers;

use App\CalendarClass\CalendarInterface\EventClassInterface;
use App\CalendarClass\EventClass;
use App\CalendarClass\ReminderClass;
use App\Http\RemoteCall;
use App\Models\EventInstanceException;
use App\Repo\CalendarInterface;
use App\Repo\CalendarUserInterface;
use App\Repo\Eloquent\CalendarRepo;
use App\Repo\Eloquent\CalendarUserRepo;
use App\Repo\Eloquent\EventInstanceExceptionRepo;
use App\Repo\Eloquent\EventModelRepo;
use App\Repo\Eloquent\EventPermissionRepo;
use App\Repo\Eloquent\ReminderRepo;
use App\Repo\Eloquent\RolesRepo;
use App\Repo\Eloquent\UserGroupsRepo;
use App\Repo\Eloquent\UserRepo;
use App\Repo\EventInstanceExceptionInterface;
use App\Repo\EventModelInterface;
use App\Repo\EventPermissionsInterface;
use App\Repo\ReminderInterface;
use App\Repo\RolesInterface;
use App\Repo\UserGroupsInterface;
use App\Repo\UserInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{



    public function boot(){
        \Dusterio\LumenPassport\LumenPassport::routes($this->app);

        Validator::extend('domain_validation', function($attribute, $value, $parameters, $validator) {
            $domains = explode("@",$value);
            if(checkdnsrr($domains[1],"MX")){
                return true;
            }
            else{
                return false;
            }
        });

        Validator::extendImplicit('birthday_validation', function($attribute, $value, $parameters, $validator) {
            $now = Carbon::now();
            $date = Carbon::parse($value);
            $diff = $date->diffInYears($now);
            $ageLimit = Config::get("config.age_limit");
            if($now->gt($date) && ($diff >= $ageLimit)) return true;
            else return false;
        });

    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ReminderInterface::class,ReminderRepo::class);
        $this->app->bind(\App\CalendarClass\CalendarInterface\ReminderInterface::class,ReminderClass::class);
        $this->app->bind(EventClassInterface::class,EventClass::class);
        $this->app->bind(EventModelInterface::class,EventModelRepo::class);
        $this->app->bind(EventInstanceExceptionInterface::class,EventInstanceExceptionRepo::class);
        $this->app->bind(UserInterface::class,UserRepo::class);
        $this->app->bind(CalendarInterface::class,CalendarRepo::class);
        $this->app->bind(CalendarUserInterface::class,CalendarUserRepo::class);
        $this->app->bind(UserGroupsInterface::class,UserGroupsRepo::class);
        $this->app->bind(RolesInterface::class,RolesRepo::class);

        $this->app->bind(EventPermissionsInterface::class,EventPermissionRepo::class);

        $this->app->bind('remoteCall',RemoteCall::class);
    }
}
