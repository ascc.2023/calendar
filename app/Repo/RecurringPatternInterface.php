<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/18/19
 * Time: 3:11 PM
 */

namespace App\Repo;


interface RecurringPatternInterface
{

    public function getSpecificPattern(int $reminder_id);

    public function getAllEventPatterns(int $event_id);

    public function getEventSpecificPattern(int $event_data, int $pattern_id);

    public function createPattern(array $data);

    public function createParentAndChildPattern($event_data);

    public function updatePattern( $pattern , array $data);

    public function updatePatternByPatternId($pattern_id, array  $data);

    public function updatePatternByPatternAndEventId($pattern_id,$event_id,array $data);

    public function updatePatternByEventId(array $data);

    public function deletePattern($id);

    public function deletePatternByEventIds(array $event);
}