<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/4/19
 * Time: 9:33 AM
 */

namespace App\Repo;


interface EventInstanceExceptionInterface
{
    public function getExceptionDates(array $event_ids);

    public function addExceptionDate(array $vents, \DateTime $ex_date);

    public function removeExceptionDates(array $event_ids);
}