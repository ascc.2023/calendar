<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/19
 * Time: 5:36 PM
 */

namespace App\Repo;


interface RolesInterface
{
    public function getSpecificRole($value);

    public function getAllRoles($limit);

    public function createRole(array $data);

    public function updateRole($id,array $data);

    public function deleteRole($id);
}