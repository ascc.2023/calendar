<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/19
 * Time: 12:53 PM
 */

namespace App\Repo;


interface UserGroupsInterface
{
    public function getUserGroups($value,$status);

    public function getAllUserGroups($status,$limit);

    public function createUserGroups(array $data);

    public function updateUserGroups($id, array $data);

    public function deleteUserGroups($id);
}