<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/19/19
 * Time: 10:39 AM
 */

namespace App\Repo;


interface EventModelInterface
{
    public function getSpecificEvent( $event_id, $show_child);

    public function getAllEvent($user_email,$start_date, $end_date);

    public function getAllUserEventsByCalendars($user_email,$start_date, $end_date, array $calendars);

    public function getEventsByCalendarId($email,$user_calendar_id);

    public function checkPermissionExists($name);

    public function createEvent(array $event);

    public function updateEvent( $event_id, array $events);

    public function deleteEvent($event_id);
}