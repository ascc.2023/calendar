<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/26/19
 * Time: 5:17 PM
 */

namespace App\Repo;


interface CalendarUserInterface
{
    public function getCalendarUser($user_id,$status,array $name,array $select);

    public function deleteUserCalendar($id,$user_id);

    public function updateUserCalendar($id, $usr_id,array $data);

    public function createOrFetchCalendars($name,$users);
}