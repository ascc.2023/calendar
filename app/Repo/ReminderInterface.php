<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 1:02 PM
 */

namespace App\Repo;


interface ReminderInterface
{
    /**
     * Fetch reminder based on @param $reminder_id
     * @param int $reminder_id
     * @return mixed
     *
     */
    public function getReminder(int $reminder_id);

    /**
     * List all the reminders related to certain event
     * @param int $event_id
     * @return mixed
     */
    public function getAllEventReminders(int $event_id);

    /**
     * Fetch specific reminder of an event
     * @param int $event_id
     * @param int $reminder_id
     * @return mixed
     */
    public function getEventSpecificReminder(int $event_id, int $reminder_id);

    /**
     * creates event reminder
     * @param array $data
     * @return mixed
     */
    public function createReminder(array $data);

    /**
     * update reminder
     * @param $reminder
     * @param array $data
     * @return mixed
     */
    public function updateReminder( $reminder , array $data);

    /**
     * update reminder by reminder id provided
     * @param $reminder_id
     * @param array $data
     * @return mixed
     */
    public function updateReminderByReminderId($reminder_id, array  $data);

    /**
     * update reminder by reminder and event id
     * @param $reminder_id
     * @param $event_id
     * @param array $data
     * @return mixed
     */
    public function updateReminderByReminderAndEventId($reminder_id,$event_id, array $data);

    /**
     * remove reminer from database
     * @param $id
     * @return mixed
     */
    public function deletedReminder($id);

    /**
     * Remove all reminders of specific event of event from reminders table
     * @param $event_id
     * @return mixed
     */
    public function deleteRemindersByEventId($event_id);

    /**
     * Function to remove reminder Id passed as a parameter
     * @param array $ids
     * @return mixed
     */
    public function deleteRemindersByIds(array  $ids);


}