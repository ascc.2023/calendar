<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/9/19
 * Time: 10:16 AM
 */

namespace App\Repo;


interface EventPermissionsInterface
{
    public function listPermissions();

    public function getPermission($id);

    public function createPermission(array $request);

    public function updatePermission($id, array $request);

    public function deletePermission($id);


}