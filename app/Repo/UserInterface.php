<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/19/19
 * Time: 12:53 PM
 */

namespace App\Repo;


interface UserInterface
{
    public function getSpecificUser($value,$status);

    public function getSpecificUserWithoutStatus($value);

    public function listUsers($request);

    public function getUserByEmails(array $emails);

    public function createUser(array $request);

    public function updateUser($id, array $request);

    public function deleteUser($id);
}