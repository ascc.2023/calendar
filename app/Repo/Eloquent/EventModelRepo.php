<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/19/19
 * Time: 10:42 AM
 */

namespace App\Repo\Eloquent;


use App\Repo\EventModelInterface;
use App\Models\Event;
use Illuminate\Support\Facades\DB;

class EventModelRepo implements EventModelInterface
{
    private $event;
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    //query to retrive all the childs including parents
    //select  *
    //from    (select * from events
    //order by parent_event_id, id) products_sorted,
    //(select @pv := '7') initialisation
    //where   (find_in_set(parent_event_id, @pv) or find_in_set(id, @pv))
    //and     length(@pv := concat(@pv, ',', id))




    //query to retrive all the parents record
//    SELECT T2.id, T2.title
//    FROM (
//    SELECT
//    @r AS _id,
//    (SELECT @r := parent_event_id FROM events WHERE id = _id) AS parent_id,
//    @l := @l + 1 AS lvl
//    FROM
//    (SELECT @r := 18, @l := 0) vars,
//    events h
//    WHERE @r <> 0) T1
//    JOIN events T2
//    ON T1._id = T2.id
//    ORDER BY T1.lvl DESC

    public function getSpecificEvent( $event_id, $show_child = false)
    {
//        dd($show_child);
        if($show_child){
            return $this->event->with('AllChildEvents')->findOrFail($event_id);
        }
        return $this->event->findOrFail($event_id);
    }

    /**
     * returns all the child nodes of parent node
     * @param $parent_id
     * @return array
     */
    public function fetchChildsOfParent($parent_id){
        return DB::select('
        select  *
        from    (select * from events
         order by parent_event_id, id) event_sorted,
        (select @pv := :id) initialisation
        where   (find_in_set(parent_event_id, @pv))
        and     length(@pv := concat(@pv, \',\', id))
        ',['id' => $parent_id]);
    }

    public function getAllEvent($user_email, $start_date, $end_date)
    {
        return $this->event->where('created_by',$user_email)
            ->where(function ($q) use ($start_date,$end_date){
                $q->whereBetween('start_date',[$start_date,$end_date])
                    ->orwhereBetween('recurring_end_date' ,[$start_date,$end_date])
                    ->orwhereBetween('end_date' ,[$start_date,$end_date])
                    ->orWhere(function ($q) use ($start_date,$end_date){
                        $q->where('start_date','<=',$start_date)->where('recurring_end_date' ,'>=',$end_date);
                    });
            })
            ->with('AllChildEvents')
            ->with('reminder')
            ->orderBY('start_date','asc')
            ->get();
//        dd(DB::getQueryLog());
    }

    public function getEventsByCalendarId($email,$user_calendar_id){
        return $this->event->where([
            ['created_by',$email],
            ['user_calendar_id',$user_calendar_id]
        ])->get();
    }

    public function getAllUserEventsByCalendars($user_email, $start_date, $end_date, array $calendars)
    {

        return $this->event->where('created_by',$user_email)
            ->whereIn('user_calendar_id',$calendars)
            ->where(function ($q) use ($start_date,$end_date){
                $q->whereBetween('start_date',[$start_date,$end_date])
                    ->orwhereBetween('recurring_end_date' ,[$start_date,$end_date])
                    ->orwhereBetween('end_date' ,[$start_date,$end_date])
                    ->orWhere(function ($q) use ($start_date,$end_date){
                        $q->where('start_date','<=',$start_date)->where('recurring_end_date' ,'>=',$end_date);
                    });
            })
//            ->orWhere()
            ->with('AllChildEvents')
            ->with('reminder')
            ->orderBY('start_date','asc')
            ->get();
    }

    public function checkPermissionExists($name)
    {
        return $this->event->where('event_permission','like','%'.$name.'%')->first();
    }


    public function createEvent(array $event)
    {
        return $this->event->create($event);
    }

    public function updateEvent($event_id, array $event)
    {
//        dd($event);

        if(isset($event['existing_child']) || isset($event['remove_child'])){
            $existing_child = $event['existing_child'];
            $remove_child = $event['remove_child'];
//            dd('hsh');
//            $event['has_reminder'] = 0;
            unset($event['guests']);
            unset($event['existing_child'],$event['remove_child']);
            if(!empty($existing_child)){
//dd($existing_child);
                $this->event->whereIn('id',$existing_child)->update($event);
            }
            if(!empty($remove_child)){
                $this->event->whereIn('id',$remove_child)->delete();
            }

        }
        $this->event->findOrFail($event_id)->update($event);
        return true;
    }


    public function deleteEvent($event_id)
    {
        return $this->event->where('id',$event_id)->orWhere('parent_event_id',$event_id)->delete();
    }

    public function deleteChildEvents($child_ids){
        return $this->event->whereIn('id',$child_ids)->delete();
    }

}