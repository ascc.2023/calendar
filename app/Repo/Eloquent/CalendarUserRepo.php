<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/26/19
 * Time: 5:18 PM
 */

namespace App\Repo\Eloquent;


use App\Exceptions\NotAllowedException;
use App\Models\CalendarUsers;
use App\Repo\CalendarInterface;
use App\Repo\CalendarUserInterface;
use Carbon\Carbon;

class CalendarUserRepo implements CalendarUserInterface
{
    private $calendarUsers;
    private $calendar;
    public function __construct(CalendarUsers $calendarUsers,CalendarInterface $calendars)
    {
        $this->calendarUsers = $calendarUsers;
        $this->calendar = $calendars;
    }

    public function getCalendarUser($user_id, $status, array $name, array $select)
    {
        $calendar_user =  $this->calendarUsers->where([
                ['user_id',$user_id],
                ['status' , $status]
            ]
        );
        if(!empty($name)){
            $calendar_user = $calendar_user->whereIn('name',$name);
        }
        if(!empty($select)){
            $calendar_user = $calendar_user->get($select);
        }
        else{
            $calendar_user = $calendar_user->get();
        }
        return $calendar_user;

    }

    public function updateUserCalendar($id,$user_id,array $data)
    {
//        dd($id);
        $user_calendar =  $this->calendarUsers->where('user_id',$user_id)->findOrFail((int)$id);
        if(isset($data['name']) && (strtolower(trim($data['name']))) != strtolower(trim($user_calendar->name))){
            if($user_calendar->is_modifiable == 1){

//                $calendar = $this->calendar->createCalendar($data);
//                $create_user_calendar = $this->calendarUsers->create([
//                    'user_id' => $user_id,
//                    'calendar_id' => $calendar->id,
//                    'status' => 1,
//                    'name' => $data['name']
//                ]);
//                return $user_calendar->delete();
            }
            else{
                throw new NotAllowedException('Non modifiable calendar name can not be updated.');
            }


        }
        return $user_calendar->update($data);
    }


    public function deleteUserCalendar($id,$user_id)
    {
//        $user_calendar =  $this->calendarUsers->where('user_id',$user_id)->findOrFail($id);
        return $this->calendarUsers->where('user_id',$user_id)->findOrFail($id)->delete();
    }

    public function createOrFetchCalendars($organiser_calendar, $users)
    {
//        dd($users);
        $calendars = $this->calendarUsers->where('name',$organiser_calendar->name)->whereIn('user_id',$users)->get();
        $calendar_having_users = $calendars->pluck('user_id')->all();
        $users_not_having_calendars = array_diff($users,$calendar_having_users);

//dd($users_not_having_calendars);
        if(empty($users_not_having_calendars) || count($users_not_having_calendars) != 0){
            $data = [];
            $now = Carbon::now('utc');
            foreach ($users_not_having_calendars as $user){
                $data[] = [
                    'name' => $organiser_calendar->name,
                    'calendar_id' => $organiser_calendar->calendar_id,
                    'user_id' => $user,
                    'color' => $organiser_calendar->color,
                    'status' => 1,
                    'is_modifiable' => 1,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
            $this->calendarUsers->insert($data);
             $calendars = $this->calendarUsers->where('name',$organiser_calendar->name)->whereIn('user_id',$users)->get();
        }
        return $calendars;

    }


}