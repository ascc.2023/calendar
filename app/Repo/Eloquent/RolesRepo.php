<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/19
 * Time: 5:38 PM
 */

namespace App\Repo\Eloquent;


use App\Models\Roles;
use App\Repo\RolesInterface;

class RolesRepo implements RolesInterface
{
    private $role;
    public function __construct(Roles $role)
    {
        $this->role = $role;
    }

    public function getSpecificRole($value)
    {
       return $this->role->where(function ($query) use ($value){
           $query->where('name',$value)->orWhere('id',$value);
       })->firstOrfail();
    }

    public function getAllRoles($limit)
    {
        return $this->role->paginate($limit);
    }

    public function createRole(array $data)
    {
       return $this->role->create($data);
    }

    public function updateRole($id, array $data)
    {
       return $this->role->findOrFail($id)->update($data);
    }

    public function deleteRole($id)
    {
        return $this->role->findOrFail($id)->delete();
    }

}