<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/4/19
 * Time: 9:37 AM
 */

namespace App\Repo\Eloquent;


use App\Models\EventInstanceException;
use App\Repo\EventInstanceExceptionInterface;

class EventInstanceExceptionRepo implements EventInstanceExceptionInterface
{
    private $event_exception;
    public function __construct(EventInstanceException $event_exception)
    {
        $this->event_exception = $event_exception;
    }

    public function getExceptionDates(array $event_ids)
    {
       return $this->event_exception->whereIn('event_id',$event_ids)->orderBy('ex_date','asc')->get();
    }

    /**
     * Add a recurrence exception date for the given event id
     * @param array $events
     * @param \DateTime $ex_date
     * @return mixed
     */
    public function addExceptionDate(array $events, \DateTime $ex_date)
    {
        $ex_date = $ex_date->format('Y-m-d H:i:s');
        $exeception_data = [];
        foreach ($events as $event){
         $exeception_data[] = [
             'event_id' => $event,
             'ex_date' => $ex_date
         ];
        }
        //fetch the events which has the same date as requested date ,if exist remove it from inserting otherwise insert
        $check_for_exceptions = $this->event_exception->whereIn('event_id',$events)->whereIn('ex_date',[$ex_date])->get();
        foreach ($exeception_data as $key => $exception){
            foreach ($check_for_exceptions as $db_exception){
                if($db_exception['event_id'] == $exception['event_id']){
                    unset($exeception_data[$key]);
                }
            }
        }
        $check_for_exception = $this->event_exception->insert($exeception_data);
        return $check_for_exception;
    }

    public function removeExceptionDates(array $event_ids)
    {

        return $this->event_exception->whereIn('id',$event_ids)->delete();

    }

}