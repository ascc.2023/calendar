<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/19
 * Time: 1:32 PM
 */

namespace App\Repo\Eloquent;


use App\Models\Calendars;
use App\Repo\CalendarInterface;

class CalendarRepo implements CalendarInterface
{
    private $calendar;
    public function __construct(Calendars $calendar)
    {
        $this->calendar = $calendar;
    }

    public function getCalendar($value)
    {
       return $this->calendar->where('id',$value)->orWhere('name',$value)->first();
    }

    public function getCalendarStrict($value)
    {
        return $this->calendar->where('id',$value)->orWhere('name',$value)->firstOrFail();
    }

    public function createCalendar(array $request)
    {
        $calendar = $this->getCalendar($request['name']);
        if($this->getCalendar($request['name'])){
            return $calendar;
        }
        return $this->calendar->create($request);
    }

    public function updateCalendar($value, array $request)
    {
        $calendar = $this->getCalendarStrict($value);
        return $calendar->update($request);
    }

    public function deleteCalendar($value)
    {
        return $calendar = $this->getCalendarStrict($value)->delete();
    }

}