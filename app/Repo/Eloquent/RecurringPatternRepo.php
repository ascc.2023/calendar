<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/18/19
 * Time: 3:16 PM
 */

namespace App\Repo\Eloquent;


use App\Models\RecurringPattern;
use App\Repo\RecurringPatternInterface;
use Illuminate\Support\Facades\DB;

class RecurringPatternRepo implements RecurringPatternInterface
{
    private $pattern;
    public function __construct(RecurringPattern $pattern)
    {
        $this->pattern = $pattern;
    }

    public function getSpecificPattern(int $reminder_id)
    {
        return $this->pattern->findOrFail($reminder_id);
    }

    public function getAllEventPatterns(int $event_id)
    {
        return $this->pattern->where('event_id',$event_id)
            ->join('recurring_types','recurring_types.id','=','recurring_patterns.id')
            ->select('recurring_patterns.*','recurring_types.recurring_type')
            ->get();
    }

    public function getEventSpecificPattern(int $event_id, int $pattern_id)
    {
        return $this->pattern->where([
            [ 'event_id',$event_id],
            ['id',$pattern_id]
        ])
            ->join('recurring_types','recurring_types.id','=','recurring_patterns.id')
            ->select('recurring_patterns.*','recurring_types.recurring_type')
            ->firstOrFail();
    }

    public function createPattern(array $data)
    {
        return $this->pattern->create($data);
    }

    public function createParentAndChildPattern($event_data)
    {
        $new_data = [];
        foreach ($event_data['existing_child'] as $child){
            $new_data[] = [
                "event_id" => $child,
                'recurring_type' => $event_data['recurring_type'],
                'separation_count' => $event_data['separation_count'] ,
                'max_num_of_occurrences' => $event_data['max_num_of_occurrences'],
                'day_of_week' => $event_data['day_of_week'],
                'week_of_month' => $event_data['week_of_month'],
                'day_of_the_month' => $event_data['day_of_the_month'],
                'month_of_the_year' => $event_data['month_of_the_year']
            ];
        }
        $chunked_patterns = array_chunk($new_data,990);
        foreach ($chunked_patterns as $pattern){
            $this->pattern->insert($pattern);
        }
        return true;
    }


    public function updatePattern($pattern, array $data)
    {
        return $pattern->update($data);
    }

    public function updatePatternByPatternId($pattern_id, array $data)
    {
        return $this->pattern->findOrFail($pattern_id)->update($data);
    }

    public function updatePatternByPatternAndEventId($pattern_id, $event_id, array $data)
    {

        return $this->pattern->where([
            'id' => $pattern_id,
            'event_id' => $event_id
        ])->firstOrFail()->update($data);
    }

    public function updatePatternByEventId( array $data)
    {
       foreach ($data['existing_child'] as $child){
           $new_data = [
               'recurring_type' => $data['recurring_type'],
               'separation_count' => $data['separation_count'] ,
               'max_num_of_occurrences' => $data['max_num_of_occurrences'],
               'day_of_week' => $data['day_of_week'],
               'week_of_month' => $data['week_of_month'],
               'day_of_the_month' => $data['day_of_the_month'],
               'month_of_the_year' => $data['month_of_the_year']
           ];
           $this->pattern->where('event_id', $child)->update($new_data);
       }
       return true;
    }


    public function deletePattern($id)
    {
        return $this->pattern->findOrFail($id)->delete();
    }

    public function deletePatternByEventIds(array $event)
    {
        return $this->pattern->whereIn('event_id',$event)->delete();
    }


}