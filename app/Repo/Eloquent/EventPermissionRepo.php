<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/9/19
 * Time: 5:20 PM
 */

namespace App\Repo\Eloquent;


use App\Models\EventPermission;
use App\Repo\EventPermissionsInterface;

class EventPermissionRepo implements EventPermissionsInterface
{
    private $event_permission;
    public function __construct(EventPermission $event_permission)
    {
        $this->event_permission = $event_permission;
    }

    public function getPermission($id)
    {
        return $this->event_permission->findOrFail($id);
    }


    public function listPermissions()
    {
      return $this->event_permission->all();
    }

    public function createPermission(array $request)
    {
      return $this->event_permission->create($request);
    }

    public function updatePermission($id, array $request)
    {
       return $this->event_permission->findOrFail($id)->update($request);
    }

    public function deletePermission($id)
    {
        return $this->event_permission->delete();
    }

}