<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/19/19
 * Time: 12:53 PM
 */

namespace App\Repo\Eloquent;


use App\Models\User;
use App\Repo\UserInterface;

class UserRepo implements UserInterface
{
    private $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getSpecificUser($value, $status = 1)
    {
        return $this->user->where('status',$status)->where(function ($query) use ($value){
            $query->where('id',$value)->orWhere('email',$value);
        })->firstOrFail();
    }

    public function getUserByEmails(array $emails)
    {
       return $this->user->whereIn('email',$emails)->get();
    }

    public function listUsers($request)
    {
        $users = $this->user;
        if(isset($request['q'])){
            $users = $users->where('full_name','like','%'.$request['q'].'%')->orwhere('email','like','%'.$request['q'].'%');
        }
        return $users->paginate($request['limit'])->withPath('/admin/users')->appends($request);
    }


    public function getSpecificUserWithoutStatus($value){
        return $this->user->where('email',$value)->orWhere('id',$value)->first();
    }

    public function createUser(array $request)
    {
        return $this->user->create($request);
    }

    public function updateUser($id, array $request)
    {
        return $this->user->findOrFail($id)->update($request);
    }

    public function fetchUserGroups(array $emails,$group_id){
        return $this->user->join('user_groups_pivot','user_groups_pivot.user_id','=','users.id')
            ->whereIn('users.email',$emails)
            ->where('users.status',1)
            ->where('user_groups_pivot.user_group_id',$group_id)->count();
    }

    public function deleteUser($id )
    {
        $user = $this->user->findOrFail($id);
       if($user->status == 0){
           $user->delete();
           return 'hard';
       }
       else{
           $user->update([
               'status' => 0
           ]);
           return 'soft';
       }
    }

}