<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/19
 * Time: 12:56 PM
 */

namespace App\Repo\Eloquent;


use App\Models\UserGroups;
use App\Repo\UserGroupsInterface;

class UserGroupsRepo implements UserGroupsInterface
{
    private $group;
    public function __construct(UserGroups $group)
    {
        $this->group = $group;
    }

    public function getUserGroups($value, $status)
    {
        if(is_null($status)){
            return $this->group->where('id',$value)->orWhere('name',$value)->firstOrfail();
        }
        else{
            return $this->group->where('status',$status)->where(function ($query) use ($value,$status){
                $query->where('id',$value)->orWhere('name',$value);
            })->firstOrfail();
        }
    }

    public function getAllUserGroups($status,$limit)
    {
        if(is_null($status)){
            return $this->group->paginate($limit);
        }
        else{
            return $this->group->where('status',$status)->paginate($limit);
        }

    }

    public function createUserGroups(array $data)
    {
        return $this->group->create($data);
    }

    public function updateUserGroups($id, array $data)
    {
        return $this->group->findOrfail($id)->update($data);
    }

    public function deleteUserGroups($id)
    {
       return $this->group->findOrFail($id)->delete();
    }

}