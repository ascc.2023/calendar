<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 1:11 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\ReminderInterface;
use App\Models\Reminder;
class ReminderRepo implements ReminderInterface
{
    private $reminder;
    public function __construct(Reminder $reminder)
    {
        $this->reminder = $reminder;
    }

    public function getReminder(int $reminder_id)
    {
        return $this->reminder->findOrFail($reminder_id);
    }

    public function getAllEventReminders(int $event_id)
    {
        return $this->reminder->where('event_id',$event_id)->orderBy('created_at','desc')->get();
    }

    public function getEventSpecificReminder(int $event_id, int $reminder_id)
    {
       return $this->reminder->where([
           'event_id',$event_id,
           'id',$reminder_id
       ])->firstOrFail();
    }

    public function createReminder(array $data)
    {
       return $this->reminder->create($data);
    }


    public function updateReminder($reminder, array $data)
    {
       return $reminder->update($data);
    }

    public function updateReminderByReminderId($reminder_id, array  $data){
        return $this->reminder->findOrFail($reminder_id)->update($data);
    }

    public function updateReminderByReminderAndEventId($reminder_id, $event_id, array $data)
    {
      return $this->reminder->where([
          ['id' , $reminder_id],
          ['event_id',$event_id]
      ])->firstOrfail()->update($data);

    }


    public function deletedReminder($id)
    {
        return $this->reminder->findOrFail($id)->delete();
    }

    public function deleteRemindersByEventId($event_id)
    {
        return $this->reminder->where('event_id',$event_id)->delete();
    }

    public function deleteRemindersByIds(array $ids)
    {
        return $this->reminder->whereIn('id',$ids)->delete();
    }


}