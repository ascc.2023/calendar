<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/19
 * Time: 1:29 PM
 */

namespace App\Repo;


interface CalendarInterface
{
    public function getCalendar($value);

    public function getCalendarStrict($value);

    public function createCalendar(array $request);

    public function updateCalendar($id, array $request);

    public function deleteCalendar($id);


}