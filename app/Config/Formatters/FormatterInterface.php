<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/31/19
 * Time: 4:36 PM
 */

namespace App\Config\Formatters;


interface FormatterInterface
{
    public function setFormat($data);
}