<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/31/19
 * Time: 4:31 PM
 */

namespace App\Config;


use App\Config\Formatters\FormatterInterface;

class CalendarConfig
{
    /** @var DateTime */
    private $start_date;
    /** @var DateInterval */
    private $interval;
    /** @var formatters\FormatterInterface */
    private $formatter;
    private $end_date;
    public $include_quarters = true;
    public $include_weeks = true;

    public function setFormatter( FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    public function setInterval(\DateInterval $interval)
    {
        $this->interval = $interval;
    }

    public function getInterval()
    {
        return $this->interval;
    }

    public function getFormatter()
    {
        return $this->formatter;
    }

    public function setStartDate(\DateTime $date)
    {
        $this->start_date = $date;
    }

    public function setEndDate(\DateTime $date){
        $this->end_date = $date;
}

    public function getStartDate(){
        return $this->start_date;
    }
    public function getEndDate(){
        return $this->end_date;
    }
}