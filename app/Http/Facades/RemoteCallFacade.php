<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/22/19
 * Time: 10:58 AM
 */

namespace App\Http\Facades;


use Illuminate\Support\Facades\Facade;

class RemoteCallFacade extends Facade
{
    protected static function getFacadeAccessor(){
        return 'remoteCall';
    }
}