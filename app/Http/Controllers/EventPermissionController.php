<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/10/19
 * Time: 12:25 PM
 */

namespace App\Http\Controllers;


use App\Repo\EventModelInterface;
use App\Repo\EventPermissionsInterface;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class EventPermissionController extends Controller
{
    private $event_permission;
    private $event;
    private $slugify;
    public function __construct(EventPermissionsInterface $event_permission, EventModelInterface $event,Slugify $slugify)
    {
        $this->event_permission = $event_permission;
        $this->event = $event;
        $this->slugify = $slugify;
    }


    public function listPermission(){
        try{
            return response()->json([
                'status' => '200',
                'data' => $this->event_permission->listPermissions()
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error listing event permission.'
            ]);
        }

    }

    public function listPermissionsForPublic(){
        try{
            return response()->json([
                'status' => '200',
                'data' => $this->event_permission->listPermissions()
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error listing event permission.'
            ]);
        }
    }

    public function createPermission(Request $request){
        try{
            if($request->has('name')){
                $sluged_value = $this->slugify->slugify($request->name,['regexp' => '/([^A-Za-z0-9]|-)+/']);
                $request->merge([
                    'name' => $sluged_value
                ]);
            }
            $this->validate($request,[
                'name' => 'required|unique:event_permissions,name',
                'display_name' => 'string'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ]);
        }
        try{
            $this->event_permission->createPermission($request->only('name','display_name'));
            return response()->json([
                'status' => '200',
                'message' =>  'Event permission created successfully.'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error creating permission'
            ]);
        }
    }

    public function updateEventPermission(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'display_name' => 'required|string'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ]);
        }
        try{
            $this->event_permission->updatePermission($id,$request->only('display_name'));

            return response()->json([
                'status' => '200',
                'message' => 'Event permission updated successfully.'
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => 'Event permission could not be found.'
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error updating event permission.'
            ],404);
        }
    }

    public function deleteEventPermission($id){
        try{
            $permission = $this->event_permission->getPermission($id);
            $event = $this->event->checkPermissionExists($permission->name);
            if($event){
                return response()->json([
                    'status' => '403',
                    'message' => 'The permission exist in one or more of the event.'
                ],403);
            }
            $this->event_permission->deletePermission($id);
            return response()->json([
                'status' => '200',
                'message' => 'Event permission deleted successfully.'
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => 'Event permission could not be found.'
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error deleting event permission.'
            ],500);
        }
    }

}