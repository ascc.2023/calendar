<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 3:34 PM
 */

namespace App\Http\Controllers;


use App\CalendarClass\ReminderClass;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReminderController extends Controller
{
    private $reminder;
    private $log;
    public function __construct(ReminderClass $reminder, \LogStoreHelper $log)
    {
        $this->reminder = $reminder;
        $this->log = $log;
    }

    /**
     * Fetch all the reminders from the event id provided
     * @param $event_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEventReminder($event_id){
        try{
            $reminders = $this->reminder->getReminders($event_id);
            return response()->json([
                'status' => '200',
                'data' => $reminders
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                'event-reminder-fetch-error',[
                    'event_id' => $event_id,
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '404',
                'message' => 'Could not find data.'
            ],404);

        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                'event-reminder-fetch-error',[
                    'event_id' => $event_id,
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Internal Server Error'
            ],500);
        }

    }

    public function createReminder(Request $request){
        DB::beginTransaction();
        try{

        }
        catch (\Exception $ex){

        }
    }
}