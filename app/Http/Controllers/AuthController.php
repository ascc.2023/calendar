<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/18/19
 * Time: 4:02 PM
 */

namespace App\Http\Controllers;


use App\Repo\CalendarInterface;
use App\Repo\UserInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;
use Lcobucci\JWT\Parser;
use RemoteCall;

class AuthController extends Controller
{
    private $user;
    private $calendar;
    public function __construct(UserInterface $user,CalendarInterface $calendar)
    {
        $this->user = $user;
        $this->calendar = $calendar;
    }

    /**
     * Calls oauth/token route by attaching client_id ,client_secret,grant_type
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $message = [
                'in' => 'The grant type field value must be either password or client_credentials'
            ];
            $this->validate($request, [
                'grant_type' => 'required|in:password,client_credentials',
                "username" => "required",
                "password" => "required_if:grant_type,password",
                "client_id" => "required_if:grant_type,client_credentials",
                'client_secret' => 'required_if:grant_type,client_credentials'
            ],$message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        DB::beginTransaction();
        try {
            $oauth_request = [];
            //if grant type is password check if the username exist in the database if not throw an exception
            $user = $this->user->getSpecificUser($request->username,1);
            $scopes = $user->roles()->pluck('name')->all();
            if(count($scopes) == 0){
                $scopes = ['customer'];
            }
            if ($user["status"] == 0) {
                return response()->json([
                    "status" => "403",
                    "message" => "User is inactive.",
                ], 403);
            }
            if($request->grant_type == 'password'){
                $userId = $user->id;//fetch user id
                $client_id = 2;
                $client_secret = 'HQyz03Cc9OBfbQhnhOq8HIXnRbkDSbEfUsd9thOH';
                $oauth_request =  [
                    'grant_type' => "password",
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                    'username' => $request->username,
                    'password' => $request->password,
                    'scope' => $scopes
                ];
                $clientInfo = DB::table("oauth_clients")->where([
                    ['id', $client_id],
                    ['secret',$client_secret],
                    ['personal_access_client',0],
                    ['password_client',1],
                    ['revoked',0]
                ])->first();
            }
            else{
                $client_id = $request->client_id;
                $client_secret = $request->client_secret;

                $oauth_request =  [
                    'grant_type' => "client_credentials",
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                    'scope' => $scopes
                ];

                $clientInfo = DB::table("oauth_clients")->where([
                    ['id', $client_id],
                    ['secret',$client_secret],
                    ['personal_access_client',0],
                    ['password_client',0],
                    ['revoked',0]
                ])->first();
            }

//check in oauth clients table to verify passed client id and client secret matches

            if (!$clientInfo) {
                return response()->json([
                    "status" => "404",
                    "message" => "Client could not be found."
                ], 404);
            }
            //call oauth/token route to check if passed credentials were correct or not
            $proxy = Request::create(
                '/oauth/token',
                'post',
                $oauth_request
            );
            $data = App::dispatch($proxy);

            if ($data->getStatusCode() == 401) {
                return response()->json([
                    "status" => "401",
                    "message" => "The credentials were incorrect."
                ], 401);
            }

            elseif ($request['grant_type'] == 'client_credentials'){
                $current_date = Carbon::now();
                $previous_date = $current_date->copy()->subDay()->startOfDay();
                //This will keep accessible for 1 day
                DB::table('oauth_access_tokens')
                    ->where('expires_at','>=',Carbon::now()->format('Y-m-d H:i:s'))
                    ->whereBetween('created_at',[$previous_date->format('Y-m-d H:i:s'),$current_date->format('Y-m-d H:i:s')])
                    ->where('client_id',$client_id)
                    ->update([
                        'revoked' => false
                    ]);
                $calendar_name = strtolower($clientInfo->name);
                $calendar = $this->calendar->createCalendar([
                    'name' => $calendar_name
                ]);
                $now = Carbon::now('utc');
                $user_calendars = $user->calendars()->where('calendar_id',$calendar->id)->first();
                if(!$user_calendars){
                    $user->calendars()->attach($calendar,[
                        'status' => 1,
                        'name' => $calendar_name,
                        'color' => '#'.strtoupper(str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)),
                        'is_modifiable' => 0,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ]);
                }
                $calendar = $this->calendar->createCalendar([
                    'name' => 'Events'
                ]);
                $now = Carbon::now('utc');
                $user_calendars = $user->calendars()->where('calendar_id',$calendar->id)->first();
                if(!$user_calendars){
                    $user->calendars()->attach($calendar,[
                        'status' => 1,
                        'name' => 'Events',
                        'color' => '#'.strtoupper(str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)),
                        'is_modifiable' => 0,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ]);
                }
            }
            DB::commit();
            return $data;
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "User could not be found. It may be due to user is not registered."
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "There was some problem logging user"
            ], 500);
        }

    }

    /**
     * This function handles login from third party application
     * @param Request $request
     * @param $provider
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginWithProvider(Request $request , $provider){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                'email' => 'required|email',
                'password' => 'required|string|max:255',
                'client_id' => 'required|integer',
                'client_secret' => 'required|string'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
//            dd('here');
            $base_url = '';
            $user_info = [];
            $oauth_client = DB::table('oauth_clients')->where([
                ['id',$request->client_id],
                ['secret',$request->client_secret],
                ['name',strtoupper($provider)],
                ['personal_access_client',0],
                ['password_client',0],
                ['revoked',0]
            ])->first();
            if(!$oauth_client){
                return response()->json([
                    'status' => '404',
                    'message' => "$provider not found."
                ],404);
            }
            if(empty($oauth_client->redirect)){
                return response()->json([
                    'status' => '403',
                    'message' => "$provider are not allowed to login."
                ],403);
            }
            $base_url = $oauth_client->redirect;
            $full_url = $base_url .'/login';
            $remote_call = RemoteCall::call($full_url,'post',$request->only('email','password'));
            if($remote_call['status'] != 200){
                if($remote_call['status'] == 401){
                    return response($remote_call['message'],401);
                }
                return response()->json([
                    'status' => $remote_call['status'],
                    'message' => $remote_call['message']
                ],$remote_call['status']);
            }
            $data = $remote_call['message'];
            $access_token =  $data['access_token'];
            $parse_access_token = (new Parser())->parse($access_token);

            if($parse_access_token->hasClaim('params')){
                $user_info = $parse_access_token->getClaim('params');
                $company_id = $user_info->company_id;
                $request->merge([
                    'social_login' => "$provider:$company_id"
                ]);
            }
            $token = $this->createOrGetUser($request->only('email','social_login'),$provider,$request->client_id);
            DB::commit();
            return $token;
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => "Error login from $provider."
            ],500);
        }
    }

    /**
     * This will create or update user and create token
     * @param array $request
     * @param $provider
     * @return mixed
     */
    private function createOrGetUser(array $request,$provider,$client_id){
        $user = $this->user->getSpecificUserWithoutStatus($request['email']);
        if(!$user){
            $request['status'] = 1;
            $request['address'] = '';
            $user = $this->user->createUser($request);
        }
        else{
            if(strpos($user->social_login,$provider) !== false){
                $social_logins = explode(',',$user->social_login);

                foreach ($social_logins as $key => $login){
                    if(strpos($login,$provider) !== false){
                        $social_logins[$key] = $request['social_login'];
                        break;
                    }
                }
                $request['social_login'] = implode(',',$social_logins);

            }
            else{
                $request['social_login'] = $user->social_login .','.$request['social_login'];

            }
            $user->update($request);
        }
        $calendar = $this->calendar->createCalendar([
            'name' => $provider
        ]);
        $now = Carbon::now('utc');
        $user_calendars = $user->calendars()->where('calendar_id',$calendar->id)->first();
        if(!$user_calendars){
            $user->calendars()->attach($calendar,[
                'status' => 1,
                'name' => $provider,
                'color' => '#'.strtoupper(str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)),
                'is_modifiable' => 0,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
        $calendar = $this->calendar->createCalendar([
            'name' => 'Events'
        ]);
        $now = Carbon::now('utc');
        $user_calendars = $user->calendars()->where('calendar_id',$calendar->id)->first();
        if(!$user_calendars){
            $user->calendars()->attach($calendar,[
                'status' => 1,
                'name' => 'Events',
                'color' => '#'.strtoupper(str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)),
                'is_modifiable' => 0,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }


        return $user->getBearerTokenByUser($client_id,false);
//        dd($token);
//        $parsed = (new Parser())->parse($token['access_token'])->getClaim('aud');
//        dd($parsed);
    }

    /**
     * This will create a new client
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createClient(Request $request){
        $name_exists = false;
        $oauth_client = false;
        try{
            if($request->has('name')){
//                $request->merge([
//                    'name' => strtoupper($request->name)
//                ]);
                $oauth_client = DB::table('oauth_clients')->where('name',$request->name)->first();
                if($oauth_client){
                    $name_exists = true;
                }
            }
            if($name_exists){
                $this->validate($request,[
//                    'name' => 'required|string|unique:oauth_clients',
                    'redirect' => 'required|active_url',
                    'code' => 'required|string'
                ]);
            }
            else{
                $this->validate($request,[
                    'name' => 'required|string|unique:oauth_clients',
                    'redirect' => 'required|active_url',
                    'code' => 'required|string'
                ]);
            }

        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        try{
            $top_secret = '$2y$12$SAQkg/9EZK6euSz.YmjKXuaPMxc5qYt5W1/5Taom4.vHo2lwG4QMG';//genius_system@##;
            if(!Hash::check('genius_system@##',$request->code)) {
                return response()->json([
                    'status' => '403',
                    'message' => 'Forbidden'
                ], 403);
            }
            $parse_url = parse_url($request->redirect);
            if($parse_url !== false && !($parse_url['scheme'] == 'http' || $parse_url['scheme'] == 'https')){
                return response()->json([
                    'status' => '403',
                    'message' => 'Invalid redirect url.'
                ],403);
            }
            if($name_exists){
                return response()->json([
                    'status' => '200',
                    'message' => 'Client created successfully.',
                    'data' => [
                        'name' => $request->name,
                        'client_id' => $oauth_client->id,
                        'client_secret' => $oauth_client->secret
                    ]
                ]);
            }
            $string = str_random('40').uniqid();
            $secret = base64_encode(hash_hmac('sha256',$string, '##aa', true));
            $request->merge([
                'secret' => $secret,
                'user_id' => null,
                'personal_access_client' => 0,
                'password_client' => 0,
                'revoked' => 0
            ]);
            $client = Passport::client()->forceFill($request->only('secret','user_id','personal_access_client','password_client','revoked','name','redirect'));

            $client->save();
            $id = $client->id;
            return response()->json([
                'status' => '200',
                'message' => 'Client created successfully.',
                'data' => [
                    'name' => $request->name,
                    'client_id' => $id,
                    'client_secret' => $secret
                ]
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '500',
                'message' => 'Error creating an app.'
            ],500);
        }
    }
}