<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/19/19
 * Time: 12:37 PM
 */

namespace App\Http\Controllers;


use App\Repo\CalendarInterface;
use App\Repo\RolesInterface;
use App\Repo\UserInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Lcobucci\JWT\Parser;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class UserController extends Controller
{
    private $user;
    private $grant_type;
    private $calendar;

    private  $role;
    public function __construct(UserInterface $user , CalendarInterface $calendar,RolesInterface $role)
    {
        $this->user = $user;
        $this->grant_type = Config::get('config.grant_type','');
        $this->calendar = $calendar;
        $this->role = $role;
    }

    /**
     * Route to view user detail
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDetail($id){
        try{
            if($this->grant_type == 'password' && Auth::id() != $id){
                return response()->json([
                    'status' => '403',
                    'message' => 'You are allowed to view your profile only.'
                ],403);
            }
            $user = $this->user->getSpecificUser($id,1);
            return response()->json([
                'status' => '200',
                'data' => $user
            ]);
        }
        catch (ModelNotFoundException $exception){
            Log::error('error-viewing-user',[
                'id' => $id,
                'message' => $exception->getMessage()
            ]);
            return response()->json([
                'status' => '404',
                'message' => 'User could not be found.'
            ],404);
        }
        catch (\Exception $exception){
            Log::error('error-viewing-user',[
                'id' => $id,
                'message' => $exception->getMessage()
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error Updating User'
            ],500);
        }
    }

    public function listAllUser(Request $request){
        try{
            $this->validate($request,[
                'limit' => 'required|integer|min:1'
            ]);
        }
        catch (\Exception $exception){
            $request->merge([
                'limit' => 10
            ]);
        }

        try{
            $this->validate($request,[
                'q' => 'string'
            ]);
        }
        catch (\Exception $exception){
           return response()->json([
               'status' => '422',
               'message' => $exception->response->original
           ],422);
        }
        try{
           $users = $this->user->listUsers($request->all());

           return response()->json([
               'status' => '200',
               'data' => $users
           ]);
        }
        catch (\Exception $exception){

        }

    }


    public function userGroup(){
        try{
            if($this->grant_type == 'password'){
                $user = Auth::user();
            }
            else{
                $user_id = Config::get('config.user_id');
                $user = $this->user->getSpecificUser($user_id,1);
            }

            $groups = $user->userGroups;
            $users = [];
            foreach ($groups as $group){
                $group['users'] = $group->users()->where('users.id','!=',$user->id)->get(['users.full_name','users.email']);
            }
            return response()->json([
                'status' => '200',
                'data' => $groups
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => 'User Could not be found.'
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error listing user groups.'
            ]);
        }


    }

    /**
     * Route to create new user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createUser(Request $request){
        DB::beginTransaction();
        try{
            $request->merge([
                'username' => trim($request['username'])
            ]);
            $ageLimit = Config::get('config.age_limit');
            $message =[
                "domain_validation" => "The mail server for domain does not exist.",
                "birthday_validation" => "Your age must be at least $ageLimit or more."
            ];
            $this->validate($request,[
                'full_name' => 'required|string',
                'email' => 'required|email|unique:users|max:255',
//                'username' => 'required|alpha_dash|unique:users',
                'password' => ['required','string','max:255','min:6'],
                'gender' => 'required|in:male,female,other',
                'mobile' => 'required|digits:10',
                'birthday' => 'required|date_format:Y-m-d|birthday_validation',
                'address' => 'required|string',
                'avatar' =>  'mimes:jpg,jpeg,png|max:4096',
                'status' => 'boolean',
            ],$message);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            if(!$request->has('status')){
                $request->merge([
                    'status' => 1
                ]);
            }
//            if($request->has('social_login'))
            $user = $this->user->createUser($request->except('social_login'));
//            $now = Carbon::now('utc');
            $calendar = $this->calendar->createCalendar([
                'name' => 'events'
            ]);
            $now = Carbon::now('utc');
            $user_calendars = $user->calendars()->where('calendar_id',$calendar->id)->first();
            $role_customer = $this->role->getSpecificRole('customer');
            $user->roles()->attach($role_customer);
            if(!$user_calendars){
                $user->calendars()->attach($calendar,[
                    'status' => 1,
                    'name' => 'Events',
                    'color' => '#'.strtoupper(str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)),
                    'is_modifiable' => 0,
                    'created_at' => $now,
                    'updated_at' => $now,
                ]);
            }
            Log::info('user-creation',[
                'data' => $request->all(),
                'id' => $user->id,
                'message' => 'User created successfully.'
            ]);
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'User created successfully.'
            ]);
        }
        catch (\Exception $exception){
            Log::error('error-creating-user',[
                'data' => $request->all(),
                'message' => $exception->getMessage()
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error Creating User'
            ],500);
        }
    }

    /**
     * Route to update user information
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request,$id){
        try{

            $request->merge([
                'username' => trim($request['username'])
            ]);
            $ageLimit = Config::get('config.age_limit');
            $message =[
                "domain_validation" => "The mail server for domain does not exist.",
                "birthday_validation" => "Your age must be at least $ageLimit or more."
            ];
            $this->validate($request,[
                'full_name' => 'required|string',
//                'email' => 'required|email|unique:users|max:255|domain_validation',
//                'username' => 'required|alpha_dash|unique:users',
                'new_password' => ['string','max:255','min:6'],
                'password' => ['required_with:new_password','string','max:255','min:6'],
                'gender' => 'required|in:male,female,other',
                'mobile' => 'required|digits:10',
                'birthday' => 'required|date_format:Y-m-d|birthday_validation',
                'address' => 'required|string',
                'avatar' =>  'mimes:jpg,jpeg,png|max:4096',
                'status' => 'boolean'
            ],$message);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }

        try{
            $user = $this->user->getSpecificUser($id,1);
            if($this->grant_type == 'password' && (Auth::id() != $id)){
                return response()->json([
                    'status' => '403',
                    'message' => 'You are allowed to modify your profile only.'
                ],403);
            }

            if($request->has('new_password') && empty($user->social_login)){
                $user->makeVisible('password');
                if(!Hash::check($user->password,$request->password)){
                    return response()->json([
                        'status' => '403',
                        'message' => 'Password does not match.'
                    ],403);
                }
                $request->merge([
                    'password' => $request->new_password
                ]);
                $final_request =$request->only('full_name','username','password','gender','mobile','birthday','address','status');
            }
            else{
                $final_request = $request->only('full_name','username','gender','mobile','birthday','address','status');
            }
            $this->user->updateUser($id,$final_request,1);
            DB::commit();
        }
        catch (ModelNotFoundException $exception){
            Log::error('error-updating-user',[
                'id' => $id,
                'data' => $request->all(),
                'message' => $exception->getMessage()
            ]);
            return response()->json([
                'status' => '404',
                'message' => 'User could not be found.'
            ],404);
        }
        catch (\Exception $exception){
            Log::error('error-updating-user',[
                'id' => $id,
                'data' => $request->all(),
                'message' => $exception->getMessage()
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error Updating User'
            ],500);
        }
    }

    /**
     * route to delete or deactivate existing user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser($id){
        try{
            if($this->grant_type == 'password' && Auth::id() != $id){
                return response()->json([
                    'status' => '403',
                    'message' => 'You are allowed to modify your profile only.'
                ],403);
            }
            $user = $this->user->deleteUser($id);
            if($user == 'hard'){
                $messsage = 'User deleted successfully.';
            }
            else{
                $messsage = 'User deactivated successfully.';
            }
            return response()->json([
                'status' => '200',
                'message' => $messsage
            ]);
        }
        catch (ModelNotFoundException $exception){
            Log::error('error-deleting-user',[
                'id' => $id,
                'message' => $exception->getMessage()
            ]);
            return response()->json([
                'status' => '404',
                'message' => 'User could not be found.'
            ],404);
        }
        catch (\Exception $exception){
            Log::error('error-deleting-user',[
                'id' => $id,
                'message' => $exception->getMessage()
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error Deleting User'
            ],500);
        }
    }

}