<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/19
 * Time: 2:42 PM
 */

namespace App\Http\Controllers;


use App\Repo\UserGroupsInterface;
use App\Repo\UserInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserGroupsController extends Controller
{
    private $group;
    private $user;
    public function __construct(UserGroupsInterface $groups, UserInterface $user)
    {
        $this->group = $groups;
        $this->user = $user;
    }

    public function getGroups($id){
        try{
            $group = $this->group->getUserGroups($id,1);
            return response()->json([
                'status' => '200',
                'data' => $group
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error fetching data.'
            ],500);
        }
    }

    public function listGroups(Request $request){
        try{
            $request->merge([
                'limit' => $request->get('limit',10)
            ]);
            $this->validate($request,[
                'limit' => 'required|integer|min:1',
                'status' => 'required|integer|min:1'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        try{
            $get_groups = $this->group->getAllUserGroups(1,$request->limit);

            return response()->json([
                'status' => '200',
                'data' => $get_groups->withPath('/admin/groups')->appends($request->all())
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error listing group.'
            ],500);
        }
    }

    public function createGroup(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                'name' => 'required|string',
                'status' => 'required|boolean'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            $create_group = $this->group->createUserGroups($request->all());
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'User Group created successfully.'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error creating group(s).'
            ]);
        }

    }

    public function assignUserGroups(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                'user_id' => 'required|integer|exists:users,id',
                'groups' => 'required|array',
                'groups.*' => 'integer|min:1'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            $user = $this->user->getSpecificUserWithoutStatus($request->user_id);
            $user->userGroups()->sync($request->groups);
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'User successfully assigned to group(s).'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error attaching user to group(s)'
            ],500);
        }
    }

    public function assignGroupUsers(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                'users' => 'required|array',
                'users.*' => 'required|exists:users,id',
                'group_id' => 'required|integer|min:1|exists:user_groups,id',
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            $group = $this->group->getUserGroups($request->group_id,1);
            $group->users()->sync($request->users);
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Users successfully assigned to group.'
            ]);
        }

        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error attaching user to group'
            ],500);
        }
    }


    public function updateGroup(Request $request ,$id){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                'name' => 'required|string',
                'status' => 'required|boolean'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            $update_group = $this->group->updateUserGroups($id,$request->only('name','status'));
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Group updated successfully.'
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'message' => 'Could not found group.',
                'status' => '404'
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'message' => 'Error updating group.',
                'status' => '500'
            ],500);
        }
    }

    public function deleteGroup($id){
        DB::beginTransaction();
        try{
            $delete_group = $this->group->deleteUserGroups($id);
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Group deleted successfully.'
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => 'Could not found group.'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'message' => 'Error deleting group.',
                'status' => '500'
            ],500);
        }
    }

}