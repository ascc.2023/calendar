<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/19/19
 * Time: 11:37 AM
 */

namespace App\Http\Controllers;


use App\CalendarClass\EventClass;
use App\Exceptions\NotAllowedException;
use App\Models\Event;
use App\Repo\UserInterface;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use Recurr\Rule;

class EventsController extends Controller
{
    private $event;
    private $log;
    private $grant_type;
    private $user;
    public function __construct(EventClass $event, LogStoreHelper $log ,UserInterface $user)
    {
        $this->user = $user;
        $this->event = $event;
        $this->log = $log;
        $this->grant_type = Config::get('config.grant_type','');
//        dd(Config::get('config.grant_type'));
    }

    /**
     * Get specific event detail
     * @param $event_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEvent($event_id){
        try{
            $event = $this->event->getSpecificEvent($event_id);
            if($this->grant_type == 'password'){
                $user_email = Auth::user()->email;
                if($event['created_by'] !== $user_email){
                    return response()->json([
                        'status' => '403',
                        'message' => 'You can only view your event(s) only.'
                    ],403);
                }
            }
            return response()->json([
                'status' => '200',
                'data' => $event
            ]);
        }
        catch(ModelNotFoundException $ex){
            return response()->json([
                'status' => '404',
                'message' => "Event could not be found."
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '500',
                'message' => 'Error fetching event.'
            ],500);
        }

    }

    /**
     * List all the events of a specified user with the date provided
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function getUserEvents(Request $request){
        try{
            $this->validate($request,[
                'email' => 'required|email',
                'start_date' => 'required|date_format:Y-m-d',
                'end_date' => 'required|date_format:Y-m-d|after:start_date'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        if($this->grant_type == 'password'){
            $user_email = Auth::user()->email;
//            dd('here');
            if($request['email'] !== $user_email){
                return response()->json([
                    'status' => '403',
                    'message' => 'You can only view your event(s) only.'
                ],403);
            }
        }

        try{
            $events = $this->event->getSpecificUserEvents($request['email'],$request['start_date'],$request['end_date']);
            return response()->json([
                'status' => '200',
                'data' => $events
            ]);
        }

        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error listing events.'
            ],500);
        }
    }


    /**
     * Creates an events and its reminders and recurring pattern
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createEvent(Request $request){
        DB::beginTransaction();
//        $bs_dates = json_decode(file_get_contents(storage_path('bs_date.json')));
//        if($request->has('start_date') && $request->has('end_date') && $request->has('is_bs') && $request->is_bs == 1){
//            $start_date_c = Carbon::parse($request->start_date);
//            $end_date_c = Carbon::parse($request->end_date);
//            $start_date = $start_date_c->format('Y-m-d');
//            $end_date = $end_date_c->format('Y-m-d');
//            $start_date_time = $start_date_c->format('H:i:s');
//            $end_date_time = $end_date_c->format('H:i:s');
//            foreach ($bs_dates as $date){
//                if($start_date == $date->bs ){
//                    $start_date_en = $date->ad;
//                }
//                if($end_date == $date->bs){
//                    $end_date_en = $date->ad;
//                }
//            }
//
//            $request->merge([
//                'start_date' => $start_date_en.' '. $start_date_time,
//                'end_date' => $end_date_en. ' '. $end_date_time
//            ]);
//        }
        try{
            $message = [
                'reminder.*.format.in' => 'The reminder format must be either minutes,hours,days or weeks.',
                'reminder.*.notification_type.in' => 'The notification type must be either email or notification.',
                'recurring_type.in' => 'The :attribute must be either daily,weekly,monthly or yearly.',
                'visibility' => 'The :attribute must be either free or busy.',
                'privacy' => 'The :attribute must be either default,private,public.',
                'rsvp' => 'The :attribute must be either yes,no or maybe.'

            ];
            $this->validate($request,[
//                'parent_event_id' => 'nullable|integer|exists:events,id',
                'title' => 'required|string',
                'description' => 'string',
//                'user_calendar_id' => 'required|integer|min:1',
                'is_full_day_event' => 'required|boolean',
                'start_date' => 'required|date_format:Y-m-d H:i:s',
                'end_date' => 'required|date_format:Y-m-d H:i:s|after:start_date',
                'is_recurring' => 'required|boolean',
                'created_by' => 'required|email|string',
                'has_reminder' => 'required|boolean',
                'location' => 'string',
                'guests' => 'array',
                'guests.*' => 'email',
                'rsvp' => 'in:yes,no,maybe',
                'note' => 'string',
                'event_permission' => 'required_with:guests|array',
                'event_permission.*' => 'exists:event_permissions,name',
                'visibility' => 'required|in:free,busy',
                'privacy' => 'required|in:private,public,default',
                'status' => 'required|boolean',
                //recurring request
                "recurring_type" => "required_if:is_recurring,1|in:yearly,monthly,weekly,daily",
                "recurring_by_month" => "required_if:recurring_type,yearly|integer|min:1|max:12",
                "recurring_by_month_day" => "required_if:recurring_type,yearly|integer|min:1|max:32",
                "recurring_by_day" => "required_if:recurring_type,weekly|array|required_with:recurring_by_set_pos",
                "recurring_by_day.*" => "integer|min:0|max:6",
                "recurring_by_set_pos" => "integer|min:1",
                "recurring_by_until" => "date_format:Y-m-d",
                "recurring_count" => "integer|min:1",
                "recurring_interval" => "required_if:recurring_type,monthly,weekly,daily|integer|min:1",
                //reminders table
                'reminder' => 'required_if:has_reminder,1|array',
                'reminder.*.format' => 'required|in:minutes,hours,days,weeks',
                'reminder.*.format_value' => 'required|integer',
                'reminder.*.time' => 'required_if:is_full_day_event,1|date_format:H:i:s',
                'reminder.*.note' => 'string',
                'reminder.*.notification_type' => 'required|in:email,notification',
//            'rrule' => 'required_if:is_recurring,1|string',
                'duration' => 'integer',

            ],$message);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        try{
            if($this->grant_type == 'password'){
                $user = Auth::user();
                if($request['created_by'] !== $user->email){
                    return response()->json([
                        'status' => '403',
                        'message' => 'You can create your event only.'
                    ],403);
                }
            }
            else{
                $user_id = Config::get('config.user_id');
                $user = $this->user->getSpecificUser($user_id,1);
                if($request['created_by'] !== $user->email){
                    return response()->json([
                        'status' => '403',
                        'message' => 'You can create your event only.'
                    ],403);
                }
            }

            if($request->is_recurring == 1){
                $rule = $this->getRrule($request);
                $request->merge([
                    'rrule' => $rule
                ]);

            }
            else{
                $request->merge([
                    'rrule' => ''
                ]);
            }
            $user_calendar = $user->calendars()->where('calendar_users.id',$request['user_calendar_id'])->first();
            if(!$user_calendar){
                return response()->json([
                    'status' => '404',
                    'message' => 'Calendar could not be found.',
                ],403);
            }
            $request->merge([
                'calendar_detail' => $user_calendar->pivot
            ]);
            if($this->event->createEvent($request->except(
                [
                    'recurring_type','recurring_by_month','recurring_by_month_day',
                    'recurring_by_set_pos','recurring_by_until','recurring_count',
                    'recurring_interval','recurring_by_day'
                ])))
            {
                $this->log->storeLogInfo(['create-custom-event',[
                    'request' => $request->all()
                ]]);
                DB::commit();
                return response()->json([
                    'status' => '200',
                    'message' => 'Event created successfully.'
                ]);
            }

        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => $exception->getMessage()
            ],404);
        }

        catch (\Exception $ex){
            return response()->json([
                'status' => '500',
                'message' => $ex->getMessage()
            ],500);
        }
    }

//    /**
//     * @param Request $request
//     * @param $event_id
//     * @return \Illuminate\Http\JsonResponse
//     * @throws NotAllowedException
//     */

    /**
     * @param Request $request
     * @param $event_id
     * @return \Illuminate\Http\JsonResponse
     */

    public function updateEvent(Request $request,$event_id){
        DB::beginTransaction();
        try {
            $message = [
                'reminder.*.format.in' => 'The reminder format must be either minutes,hours,days or weeks.',
                'reminder.*.notification_type.in' => 'The notification type must be either email or notification.',
                'recurring_type.in' => 'The :attribute must be either daily,weekly,monthly or yearly.',
                'visibility' => 'The :attribute must be either free or busy.',
                'privacy' => 'The :attribute must be either default,private,public.',
                'rsvp' => 'The :attribute must be either yes,no or maybe.',
                'redit.in' => 'The :attribute must be either single,future,all.',
            ];
            $this->validate($request, [
                'id' => 'required',
                'title' => 'required|string',
                'user_calendar_id' => 'required|integer|min:1',
                'description' => 'string',
                'is_full_day_event' => 'required|boolean',
                'start_date' => 'required|date_format:Y-m-d H:i:s',
                'end_date' => 'required|date_format:Y-m-d H:i:s|after:start_date',
                'is_recurring' => 'required|boolean',
                'created_by' => 'required|string',
                'has_reminder' => 'required|boolean',
                'location' => 'string',
                'guests' => 'array',
                'guests.*' => 'email',
                'rsvp' => 'nullable|in:yes,no,maybe',
                'note' => 'string',
                'event_permission' => 'required_with:guests|array',
                'event_permission.*' => 'exists:event_permissions,name',
                'visibility' => 'required|in:free,busy',
                'privacy' => 'required|in:private,public,default',
                'status' => 'required|boolean',
                'recurring_end_date'  => 'nullable|date_format:Y-m-d H:i:s',
                //recurring request
                "recurring_type" => "required_if:is_recurring,1|in:yearly,monthly,weekly,daily",
                "recurring_by_month" => "required_if:recurring_type,yearly|integer|min:1|max:12",
                "recurring_by_month_day" => "required_if:recurring_type,yearly|integer|min:1|max:32",
                "recurring_by_day" => "required_if:recurring_type,weekly|array|required_with:recurring_by_set_pos",
                "recurring_by_day.*" => "integer|min:0|max:6",
                "recurring_by_set_pos" => "integer|min:1",
                "recurring_by_until" => "date_format:Y-m-d",
                "recurring_count" => "integer|min:1",
                "recurring_interval" => "required_if:recurring_type,monthly,weekly,daily|integer|min:1",

                'origid' => 'integer|min:1',
                'redit' => 'in:single,future,all',
                //reminders table
                'reminder' => 'required_if:has_reminder,1|array',
                'reminder.*.id' => 'integer|min:1',
                'reminder.*.format' => 'required|in:minutes,hours,days,weeks',
                'reminder.*.format_value' => 'required|integer',
                'reminder.*.time' => 'required_if:is_full_day_event,1|date_format:H:i:s',
                'reminder.*.note' => 'string',
                'reminder.*.notification_type' => 'required|in:email,notification',
//            'rrule' => 'required_if:is_recurring,1|string',
                'duration' => 'nullable|integer',

            ], $message);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{

            if($this->grant_type == 'password'){
                $user = Auth::user();
                if($request['created_by'] !== $user->email){
                    return response()->json([
                        'status' => '403',
                        'message' => 'You can create your event only.'
                    ],403);
                }
            }
            else{
                $user_id = Config::get('config.user_id');
                $user = $this->user->getSpecificUser($user_id,1);
                if($request['created_by'] !== $user->email){
                    return response()->json([
                        'status' => '403',
                        'message' => 'You can create your event only.'
                    ],403);
                }
            }

            if($request['is_recurring'] == 1 ){
                $exploded_id = explode('-',$event_id);
                if(( isset( $request['origid']) && $request['origid'] != $exploded_id[0]) || (  !isset( $request['origid']) && $exploded_id[0] != $request['id']) ){
                    return response()->json([
                        'status' => '403',
                        'message' => 'Orig event id field value and id value does not match.'
                    ],403);
                }
                $event_id = isset($request['origid'])? $request['origid']:$event_id;
                $rule = $this->getRrule($request);
                $request->merge([
                    'rrule' => $rule
                ]);
            }
            else{
                if(!is_numeric($request['id']) && !isset($request['origid'])){
                    return response()->json([
                        'status' => '403',
                        'message' => 'Id field value must be numeric in case of non recurring event.'
                    ],403);
                }
                if(isset($request['origid'])){
                    $event_id = $request['origid'];
                }
                else{
                    $event_id = $request['id'];
                }

            }
            $request->merge([
                'event_id' => $event_id
            ]);
            $user_calendar = $user->calendars()->where('calendar_users.id',$request['user_calendar_id'])->first();
            if(!$user_calendar){
                return response()->json([
                    'status' => '404',
                    'message' => 'Calendar could not be found.',
                ],403);
            }
            $request->merge([
                'calendar_detail' => $user_calendar->pivot
            ]);

            $this->event->editEvent($event_id,$request);
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Event updated successfully.'
            ]);

        }
        catch (NotAllowedException $ex){
            if(empty($ex->getMessage())){
                return response()->json([
                    'status' => '403',
                    'message' => 'Event type does not match with the database.'
                ],403);
            }
            return response()->json([
                'status' => '403',
                'message' => $ex->getMessage()
            ],403);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => $exception->getMessage()
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '500',
                'message' => $ex->getMessage()
            ],500);
        }
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteEvent(Request $request,$id){
        DB::beginTransaction();
        try{
            $message = [
                'redit.in' => 'The :attribute must be either single,future,all.',
            ];
            $this->validate($request,[
                'start_date' => 'required|date_format:Y-m-d H:i:s',
                'end_date' => 'required|date_format:Y-m-d H:i:s|after_or_equal:start_date',
                'is_recurring' => 'required|boolean',
                'origid' => 'required_if:is_recurring,1|integer|min:1',
                'redit' => 'required_if:is_recurring,1|in:single,future,all'
            ],$message);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        try{
            $request->merge([
                'id' => $id
            ]);
            $event_id = null;
            if($request['is_recurring'] == 1){
                $exploded_id = explode('-',$request['id']);
                if($request['origid'] != $exploded_id[0]){
                    return response()->json([
                        'status' => '403',
                        'message' => 'Orig event id field value and id value does not match.'
                    ],403);
                }
                $event_id = $request['origid'];
            }
            else{
                if(!is_numeric($request['id'])){
                    return response()->json([
                        'status' => '403',
                        'message' => 'Id field value must be numeric in case of non recurring event.'
                    ],403);
                }
                $event_id = $request['id'];
            }
            $request->merge([
                'event_id' => $event_id
            ]);

            $this->event->deleteEvent($request->all());
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Event successfully deleted.'
            ]);
        }
        catch (NotAllowedException $ex){
            return response()->json([
                'status' => '403',
                'message' => $ex->getMessage()
            ],403);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => $exception->getMessage()
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error deleting event.'
            ],500);
        }
    }

//returns the recurring pattern into ical rrule
    private function getRrule($request){
        $day = [];
        if($request->has('recurring_by_day')){
            $remove_days_repetition = array_unique($request['recurring_by_day']);
            foreach ($remove_days_repetition as $day_rep){
                switch ($day_rep){
                    case 0 :
                        $day[] = 'SU';
                        break;
                    case 1:
                        $day[] = 'MO';break;
                    case 2:
                        $day[] = 'TU'; break;
                    case 3:
                        $day[] = 'WE'; break;
                    case 4:
                        $day[] = 'TH';break;
                    case 5:
                        $day[] = 'FR'; break;
                    case 6:
                        $day[] = 'SA'; break;
                }
            }
        }
        $rule = new Rule();
        $r_rule = $rule->setFreq(strtoupper($request['recurring_type']));
        //The maximum field for ical rrule in case of yearly are by_month,by_monthday,by_day,by_set_pos,until,count
        if($request->recurring_type == 'yearly'){
            if($request->has('recurring_by_month') && $request->filled('recurring_by_month')){
                $r_rule->setByMonth([$request->recurring_by_month]);
            }
            if($request->has('recurring_by_month_day') && $request->filled('recurring_by_month_day') ){

                $r_rule->setByMonthDay([$request->recurring_by_month_day]);
            }
            if($request->has('recurring_by_day')){
                $r_rule->setByDay($day);
            }
            if($request->has('recurring_by_set_pos') && $request->filled('recurring_by_set_pos')  ){
                $r_rule->setBySetPosition([$request->recurring_by_set_pos]);
            }
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }

        }
        elseif ($request->recurring_type == 'monthly'){
            if($request->has('recurring_by_day')){
                $r_rule->setByDay($day);
            }
            if($request->has('recurring_by_set_pos') && $request->filled('recurring_by_set_pos')  ){
                $r_rule->setBySetPosition([$request->recurring_by_set_pos]);
            }
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }

            if($request->has('recurring_by_month_day') && $request->filled('recurring_by_month_day')){
                $r_rule->setByMonthDay([$request->recurring_by_month_day]);
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }
        }
        elseif ($request->recurring_type == 'weekly'){
            if($request->has('recurring_by_day')){
                $r_rule->setByDay($day);
            }
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }
        }
        else{
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }
        }


        return $r_rule->getString();


    }
}