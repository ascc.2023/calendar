<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/26/19
 * Time: 5:10 PM
 */

namespace App\Http\Controllers;


use App\CalendarClass\Calendar;
use App\CalendarClass\EventClass;
use App\Exceptions\NotAllowedException;
use App\Repo\CalendarInterface;
use App\Repo\CalendarUserInterface;
use App\Repo\EventModelInterface;
use App\Repo\UserInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CalendarController extends Controller
{
    private $calendar_user;
    private $calendar;
    private $user;
    private $grant_type;
    private $calendar_object;
    private $eventModel;
    private $eventClass;
    public function __construct(CalendarInterface $calendar,CalendarUserInterface $calendar_user,UserInterface $user,Calendar $calendar_object,EventModelInterface $eventModel,EventClass $eventClass)
    {
        $this->calendar = $calendar;
        $this->calendar_user = $calendar_user;
        $this->user = $user;
        $this->grant_type = Config::get('config.grant_type','');
        $this->calendar_object = $calendar_object;
        $this->eventModel = $eventModel;
        $this->eventClass = $eventClass;
    }


    /**
     * This route will fetch user calendars based on the user grant type .
     * if user is using grant type password it will list all the calendars or provide an option to list selected calendar.
     * However, in case of grant type client_credentials route will require an email and based on email it will list that app calendar only.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserCalendar(Request $request){

        $client_name = Config::get('config.client_name');
        $calendars = [];
        try{
            $this->validate($request,[
                'start_date' => 'date_format:Y-m-d',
                'end_date' => 'required_with:start_date|date_format:Y-m-d|after:start_date'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }

        if($request->has('start_date')){
//            $now = Carbon::now();
            $request->merge([
                'start_date' => Carbon::parse($request->start_date),
                'end_date' => Carbon::parse($request->end_date)
            ]);
        }
        else{
            $now = Carbon::now();
            $request->merge([
                'start_date' => $now->copy()->subMonth()->startOfMonth()->startOfDay(),
                'end_date' => $now->copy()->addMonth()->endOfMonth()->endOfDay()
            ]);
        }
        try{
            $this->validate($request,[
                'calendars' => 'array',
                'calendars.*' => 'string'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }

        if($this->grant_type == 'password'){

            $user = Auth::user();
            if($request->has('calendars')){
                $calendars = $request->calendars;
            }
            $calendars = $this->calendar_user->getCalendarUser($user->id,1,$calendars,['id','name','status','is_modifiable','color']);

        }
        else{
            $user = $this->user->getSpecificUser($request->email,1);
            if(!$user){
                return response()->json([
                    'status' => '404',
                    'message' => 'User does not exists.'
                ],404);
            }
            if($request->has('calendars')){
                $calendars = $request->calendars;
            }
            $calendars  = $this->calendar_user->getCalendarUser($user->id,1,$calendars,['id','name','status','is_modifiable','color']);

        }
        $fetch_calendar = $this->calendar_object->showCalendar($user->email,$calendars->toArray(),$request);
        return response()->json([
            'status' => '200',
            'data' => [
                'calendars' => $calendars,
                'events' => $fetch_calendar
            ]
        ]);
    }

    public function getAppCalendar(Request $request){
        try{
            try{
                $this->validate($request,[
                    'code' => 'required|string'
                ]);
            }
            catch (\Exception $exception){
                return response()->json([
                    'status' => '200',
                    'message' => $exception->response->original
                ],422);
            }
            $top_secret = '$2y$12$SAQkg/9EZK6euSz.YmjKXuaPMxc5qYt5W1/5Taom4.vHo2lwG4QMG';//genius_system@##;
            if(!Hash::check('genius_systems@##',$request->code)){
                return response()->json([
                    'status' => '403',
                    'message' => 'Forbidden'
                ],403);
            }

            return $oauth_clients = DB::table('oauth_clients')->where([
                ['personal_access_client',0],
                ['password_client',0],
                ['revoked',0]
            ])->get(['id as client_id','secret as client_secret','name']);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error Listing Clients.'
            ],500);
        }
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCalendar(Request $request){
        try{
            $this->validate($request,[
                'name' => 'required|string',
                'color' => 'required|string'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            if($this->grant_type == 'password'){
                $user = Auth::user();
            }
            else{
                $user = $this->user->getSpecificUser($request->email,1);
                if(!$user){
                    return response()->json([
                        'status' => '404',
                        'message' => 'User does not exists.'
                    ],404);
                }
            }

            $calendar = $this->calendar->getCalendar($request->name);
            if(!$calendar){
                $calendar = $this->calendar->createCalendar($request->only('name'));
            }
            $user_calendars = $user->calendars()->where('calendar_id',$calendar->id)->first();
            if(!$user_calendars){
                $now = Carbon::now('utc');
                $user->calendars()->attach($calendar,[
                    'status' => 1,
                    'name' => $request->name,
                    'color' => $request->color,
                    'created_at' => $now,
                    'updated_at' => $now,
                ]);
            }

            return response()->json([
                'status' => '200',
                'message' => 'Calendar Created Successfully.'
            ]);


        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error creating calendar.'
            ]);
        }
    }

    public function shareCalendar(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                'calendar_user_id' => 'required|integer|min:1|exists:calendar_users,id',
                'user_group_id' => 'required|integer|min:1|exists:user_groups,id',
                'guests' => 'required|array',
                'guests.*' => 'email',
                'permissions' => 'required|array',
                'permissions.*' => 'exists:event_permissions,name'

            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            if($this->grant_type == 'password'){
                $user = Auth::user();
            }
            else{
                try{
                    $this->validate($request,[
                        'email' => 'required|email|exists:users'

                    ]);
                }
                catch (\Exception $exception){
                    return response()->json([
                        'status' => '422',
                        'message' => $exception->response->original
                    ],422);
                }
                $user = $this->user->getSpecificUser($request->email,1);
                if(!$user){
                    return response()->json([
                        'status' => '404',
                        'message' => 'User does not exists.'
                    ],404);
                }
            }

            $request = $request->only('calendar_user_id','user_group_id','guests','permissions');
            $user_calendar = $user->calendars()->where('calendar_users.id',$request['calendar_user_id'])->first();
//            dd($user_calendar);
            if(!$user_calendar){
                return response()->json([
                    'status' => '404',
                    'message' => 'Calendar could not be found.',
                ],403);
            }


            //find the position of the logged in email passed to guests
            $position = array_search($user->email,$request['guests']);
            //if logged in user email found in the guests then remove it.
            if($position !== false){
                unset($request['guests'][$position]);
            }
            //count the total guests provide after removing creator email
            $total_guests = count($request['guests']);
            //if guests is 0 simply provide success message
            if($total_guests == 0){
                return response()->json([
                    'status' => '200',
                    'message' => 'Calendar shared successfully.'
                ],200);
            }
//db query to fetch the total guests that belongs to user group based
            $check_guests_count = $this->user->fetchUserGroups($request['guests'],$request['user_group_id']);
//            return $check_guests_count;
// if provided guests count is not equal to queries total guests then throw error
            if($total_guests != $check_guests_count){
                return response()->json([
                    'status' => '403',
                    'message' => 'One or more user does not belongs to the provided user group.'
                ],403);
            }
            $events = $this->eventModel->getEventsByCalendarId($user['email'],$request['calendar_user_id']);
//return $events;
            foreach ($events as $event){
                $permissions = $event->event_permission;
                $permissions_string = implode(',',$permissions);
//                dd($permissions);
                if(!(in_array('all',$permissions) || in_array('modify',$permissions))){
                    continue;
                }
                if(in_array('all',$permissions)){
                    $shared_permission = implode(',',$request['permissions']);
                }
                else{
                    $shared_permission = $permissions_string;
                }
//                dd($shared_permission);
                $guests = [];
                $event_guests = [];
                foreach ($request['guests'] as $guest){

                    if(array_search($guest,$event['guests']) !== false){
                        $event_guests[] = $guest;
                        continue;
                    }
                    $guests[] = $guest;
                    $event_guests[] = $guest;

                }
                $clone_event = clone $event;
                $clone_event = $clone_event->toArray();
                $clone_event['guests'] = '';
                $clone_event['event_permission'] = $shared_permission;
                $clone_event['parent_event_id'] = $clone_event['id'];
                unset($clone_event['id']);
//                return $guests;
                $this->eventClass->createChildTasksForSharing($clone_event,$guests,$user_calendar->pivot);;
                $guests = implode(',',array_unique($event_guests));
//                return $events;
                $event->update([
                    'guests' => $guests
                ]);

            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Calendar shared successfully.'
            ]);


        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error sharing calendar.'
            ],500);
        }
    }

    public function updateCalendar(Request $request,$id){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                'name' => 'required|string',
                'status' => 'required|boolean',
                'color'  => 'required|string'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '422',
                'message' => $exception->response->original
            ],422);
        }
        try{
            if($this->grant_type == 'password'){
                $user = Auth::user();
            }
            else{
                $user = $this->user->getSpecificUser($request->email,1);
                if(!$user){
                    return response()->json([
                        'status' => '404',
                        'message' => 'User does not exists.'
                    ],404);
                }
            }
//            return explode('#',$request->color);
            $this->calendar_user->updateUserCalendar($id,$user->id,$request->only('name','status','color'));
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Calendar updated successfully.'
            ]);
        }
        catch (NotAllowedException $exception){
            return response()->json([
                'status' => '403',
                'message' => $exception->getMessage()
            ],403);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => 'Calendar does not exist.'
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error updating calendar.'
            ],500);
        }

    }

    public function deleteCalendar(Request $request,$id){
        try{
            if($this->grant_type == 'password'){
                $user = Auth::user();
            }
            else{
                $user = $this->user->getSpecificUser($request->email,1);
                if(!$user){
                    return response()->json([
                        'status' => '404',
                        'message' => 'User does not exists.'
                    ],404);
                }
            }
            $delete_calendar = $this->calendar_user->deleteUserCalendar($id,$user->id);
            return response()->json([
                'status' => '200',
                'message' => 'Calendar deleted successfully.'
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => 'Calendar does not exist.'
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => 'Error deleting calendar.'
            ],500);
        }
    }

}