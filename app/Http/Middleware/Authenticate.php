<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Lcobucci\JWT\Parser;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            throw new AuthenticationException();
        }
        $token = $request->bearerToken();
        //fetch the access token id
        $parse_token = (new Parser())->parse($token);
        $id = $parse_token->getHeader('jti');
//        $client_id =
        //fetch the client id by access_token_id
        $access_token_detail = DB::table('oauth_access_tokens')->where('id',$id)->first();
        $client = DB::table('oauth_clients')->where('id',$access_token_detail->client_id)->first();
//
        if(!($access_token_detail && $client)){
            Log::error('token_not_found',['id' => $id]);
            throw new AuthenticationException();
        }
//        dd($access_token_detail);

        Config::set('config.grant_type','password');
        Config::set('config.client_id',$access_token_detail->client_id);
        Config::set('config.client_name',$client->name);
        return $next($request);
    }
}
