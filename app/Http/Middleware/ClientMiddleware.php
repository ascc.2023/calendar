<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/20/19
 * Time: 7:29 AM
 */

namespace App\Http\Middleware;


use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Exceptions\MissingScopeException;
use Lcobucci\JWT\Parser;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Validator;

class ClientMiddleware
{
    /**
     * The Resource Server instance.
     *
     * @var \League\OAuth2\Server\ResourceServer
     */
    protected $server;

    /**
     * Create a new middleware instance.
     *
     * @param  \League\OAuth2\Server\ResourceServer  $server
     * @return void
     */
    public function __construct(ResourceServer $server)
    {
        $this->server = $server;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  mixed  ...$scopes
     * @return mixed
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, \Closure $next, ...$scopes)
    {

        try{
            $validator= Validator::make($request->all(),[
                "email" => "required|email"
            ]);
            if($validator->fails()){
                throw new \Exception($validator->errors());
            }
        }
        catch (\Exception $exception){
            $message = json_decode($exception->getMessage());
            return response()->json([
                "status" => "422",
                "message" =>$message
            ],422);
        }
        try {
            $email = $request->email;
            $psr = (new DiactorosFactory())->createRequest($request);
            $psr = $this->server->validateAuthenticatedRequest($psr);
            $this->validateScopes($psr, $scopes);
            $id = $psr->getAttribute('oauth_access_token_id');
            $access_token_detail = DB::table('oauth_access_tokens')->where('id',$id)->first();
            $client = DB::table('oauth_clients')->where('id',$access_token_detail->client_id)->first();
            if(!($access_token_detail && $client)){
                Log::error('token_not_found',[
                    'id' => $id,
                    'email' => $email
                ]);
                throw new AuthenticationException();
            }
            $check_calendar_assigned_to_user = User::join('calendar_users','users.id','=','calendar_users.user_id')->where('users.email',$email)->where('users.status',1)->where('calendar_users.name',$client->name)->first();
            if(!$check_calendar_assigned_to_user){
                Log::error('calendar_not_found',[
                    'id' => $id,
                    'email' => $email
                ]);
                throw new AuthenticationException();
            }
            Config::set('config.grant_type','client');
            Config::set('config.client_id',$access_token_detail->client_id);
            Config::set('config.client_name',$client->name);
            Config::set('config.user_id',$check_calendar_assigned_to_user->user_id);
        } catch (OAuthServerException $e) {
            throw new AuthenticationException();
        }

        return $next($request);
    }

    /**
     * Validate the scopes on the incoming request.
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $psr
     * @param  array  $scopes
     * @return void
     * @throws \Laravel\Passport\Exceptions\MissingScopeException
     */
    protected function validateScopes($psr, $scopes)
    {
        if (in_array('*', $tokenScopes = $psr->getAttribute('oauth_scopes'))) {
            return;
        }

        foreach ($scopes as $scope) {
            if (! in_array($scope, $tokenScopes)) {
                throw new MissingScopeException($scope);
            }
        }
    }
}