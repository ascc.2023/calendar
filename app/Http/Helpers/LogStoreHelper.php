<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 3:05 PM
 */
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use CodeInternetApplications\MonologStackdriver\StackdriverHandler;
/**
 * Class LogStoreHelper
 */
class LogStoreHelper
{
    /**
     * LogStoreHelper constructor.
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->user_id = null;
    }

    /**
     * @param $data
     */
    public function storeLogInfo($data){
        if(ENV("LOG_STATUS") =="enable"){
            Log::info($data[0], $data[1]);
        }

    }

    /**
     * @param $data
     */
    public function storeLogError($data){
        if(ENV("LOG_STATUS") =="enable"){
            Log::error($data[0], $data[1]);
        }

    }

}