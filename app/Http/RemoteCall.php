<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/22/19
 * Time: 10:59 AM
 */

namespace App\Http;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;

class RemoteCall
{
    private $client;
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * This function will curl request to provided url
     * @param $url
     * @param $type
     * @param array $request
     * @param null $token
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function call($url,$type,array $request = [],$token = null){
        try{
            $type = strtoupper($type);
            if(!in_array($type,['GET','POST','PUT','PATCH','DELETE'])){
                return [
                    'status' => '403',
                    'message' => 'Request type must be either get,post,put,delete or patch.'
                ];
            }
            $guzzle_data['headers'] = ['Accept' => 'application/json'];
            if(!is_null($token)){
                if(strpos($token, 'Bearer') === false){
                    $token = 'Bearer ' . $token;
                }
                $guzzle_data['headers'] = [
                    'Accept' => 'application/json',
                    'Authorization' => $token
                ];
            }
            if($type != 'GET'){
                $guzzle_data['form_params'] = $request;
            }
            $result =  $this->client->request($type,$url,$guzzle_data);
            $result_data = json_decode((string) $result->getBody(), true);
            return ["message"=>$result_data,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {

            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }
    }

}