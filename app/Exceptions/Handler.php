<?php

namespace App\Exceptions;

use Exception;
use http\Exception\RuntimeException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Laravel\Passport\Exceptions\MissingScopeException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
//        if ($exception instanceof \League\OAuth2\Server\Exception\OAuthServerException && $exception->getCode() == 9) {
//            return;
//        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
//        dd('here');
        if ($exception instanceof NotFoundHttpException or $exception instanceof MethodNotAllowedHttpException) {
            // ajax 404 json feedback


            // normal 404 view page feedback
            return response()->json(
                [
                    'status'=>'404',
                    'message'=>'Requested Url Not Found'
                ], 404);

        }
        if($exception instanceof ThrottleRequestsException){
            return response()->json(
                [
                    'status'=> '409',
                    'message'=> $exception->getMessage()
                ], 409);
        }
        if($exception instanceof AuthenticationException){
            return response()->json(
                [
                    'status'=> '401',
                    'message'=> 'Unauthorised'
                ], 401);
        }
        if($exception instanceof NotAllowedException){

            if(empty($exception->getMessage())){
                return response()->json(
                    [
                        'status'=> '403',
                        'message'=>'Forbidden'
                    ], 404);
            }

            return response()->json(
                [
                    'status'=> '403',
                    'message'=> $exception->getMessage()
                ], 403);

        }
        if($exception instanceof MissingScopeException){
            return response()->json([
                'status' => '401',
                'message' => $exception->getMessage()
            ],401);
        }
        return parent::render($request, $exception);
    }
}
