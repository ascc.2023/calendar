<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/4/19
 * Time: 11:56 AM
 */

namespace App\Exceptions;


class NotAllowedException extends \Exception
{
    public function __construct($message = '')
    {
        parent::__construct($message);
    }



}