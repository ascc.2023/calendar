<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/19/19
 * Time: 10:37 AM
 */

namespace App\CalendarClass;


use App\CalendarClass\CalendarInterface\EventClassInterface;
use App\Exceptions\NotAllowedException;
use App\Models\CalendarUsers;
use App\Repo\CalendarUserInterface;
use App\Repo\EventModelInterface;
use App\Repo\UserInterface;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repo\EventInstanceExceptionInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class EventClass implements EventClassInterface
{
    private $event;
    private $reminder;
    private $base_event;
    private $max_event_instances = 8000;//day limit
    private $date_format = 'Y-m-d H:i:s';
    private $exception_format = 'Y-m-d H:i:s';
    private $rrule_date_format = 'Ymd\\THis\\Z';
    private $event_exception;
    private $user;
    private $calendar_users;
    private $grant_type;
    public $mappings = array(
        // Basic event properties. This is not all properties, only those needed explicitly
        // while processing recurrence. The others are read and set generically.
        'id' => 'id',
        'start_date' => 'start_date',
        'end_date' => 'end_date',
        'r_end_date' => 'recurring_end_date',
        // Recurrence-specific properties needed for processing recurring events:
        'rrule' => 'rrule',
        'duration' => 'duration',
        'orig_event_id' => 'origid',
        'recur_edit_mode' => 'redit',
        'recur_instance_start' => 'ristart',
        'recur_series_start' => 'rsstart',
        // Recurrence exceptions
        'exdate' => 'ex_date'
    );

    public function __construct(EventModelInterface $event,EventInstanceExceptionInterface $event_exception, UserInterface $user,ReminderClass $reminder,CalendarUserInterface $calendarUser)
    {
        $this->event = $event;
        $this->reminder = $reminder;
        $this->event_exception = $event_exception;
        $this->user = $user;
        $this->calendar_users = $calendarUser;
        $this->grant_type = Config::get('config.grant_type','');
    }


    /**
     * @param $event_id
     * @return mixed
     */
    public function getSpecificEvent($event_id)
    {

        try {

            $event = $this->event->getSpecificEvent($event_id, true);

            return $event;
        } catch (ModelNotFoundException $ex) {
            throw new ModelNotFoundException('Event could not be found.');
        }


    }

    /**
     * Returns All the user event and its member status
     * @param $user_email
     * @param null $start_date
     * @param null $end_date
     * @return mixed
     */
    public function getSpecificUserEvents($user_email, $start_date, $end_date, $calenders = [])
    {
        if(empty($calenders)){
            $events = $this->event->getAllEvent($user_email, $start_date, $end_date);
        }
        else{
            $events = $this->event->getAllUserEventsByCalendars($user_email, $start_date, $end_date,$calenders);
        }
        $matches = array();


        foreach ($events as $event) {
            $total_guest_awaiting = 0;
            $total_guest_going = 0;
            $total_guest_not_going = 0;
            $total_guest_maybe = 0;
            $rsvp_stats = [];
            $rsvp_record = [];
            if ($event->rsvp == 'yes') {
                $total_guest_going++;
            } elseif ($event->rsvp == 'no') {
                $total_guest_not_going++;
            } elseif ($event->rsvp == 'maybe') {
                $total_guest_maybe++;
            } else {
                $total_guest_awaiting++;
            }

            $rsvp_record = [
                'total_going' => $total_guest_going,
                'total_not_going' => $total_guest_not_going,
                'total_maybe' => $total_guest_maybe,
                'total_awaiting' => $total_guest_awaiting
            ];

            $rsvp_record['guests'][] = [
                'mail' => $event->created_by,
                'rsvp_status' => $event->rsvp
            ];

            $data = $event->toArray();
            if (!empty($data['all_child_events'])) { //check if the child event is empty of not if not then call function to list all the child events which are previously in tree structure to single array
                $fetch_child_event = $this->processChildEvents($data['all_child_events'], $key = 0);
                $rsvp_record = $this->getRsvpReport($fetch_child_event, $rsvp_record);// this will fetch the RSVP records like total number of awaiting, going, not going , maybe and list the guests and their rsvp status
            }
            $event->rsvp_stats = $rsvp_record;
            $event->unsetRelation('AllChildEvents'); // hide the relationship which will fetch the recurring child event

        }
        $final_events= $events->toArray();
        foreach ($final_events as $key => $event) {
            $matches = array_merge($matches,$this->generateInstances($event,$start_date,$end_date));
        }
        return $matches;

    }

    /**
     * First creates and event then create reminders and/or recurring patterns based on the types of event selected by the user.
     * @param array $event
     * @return bool|mixed
     * @throws \Exception
     */
    public function createEvent(array $event)
    {
        try {
            $has_guests = false;
            $guests = [];

            if (isset($event['guests'])) { // check if the event array contains guests index
                $search_array = array_search($event['created_by'], $event['guests']);//search in guests array to check if the organiser email exists
                if ($search_array !== false) { // if organiser mail id exists
                    unset($event['guests'][$search_array]); // remove organiser mail from the guest array
                }

                $guests = $event['guests'];
                $event['guests'] = implode(',', $guests);// convert guest array into string separated by comma (this is because guest column is of datatype string)
                count($guests) > 0 ? $has_guests = true : $has_guests = false; // if guests count is greater than 0 then multiple guest exists
            }
            $guest_permissions = isset($event['event_permission'])?$event['event_permission']:['read-only'];
            $event['event_permission'] = 'all';
            $create_event = $this->addEvent($event);
            $event['event_permission'] = implode(',',$guest_permissions);

//            dd($create_event->id);
//            dd($create_event);
            $event['guests'] = '';
            $event['event_id'] = $create_event->id;
//dd($event);
            // call functions to create reminder and recurring pattern if has_reminder and is_recurring is 1
            if ($create_event && $event['has_reminder'] == 1 && $event['is_recurring'] == 1) {
                $this->reminder->createReminder($event);// call reminder function from reminder class to create reminder(s).

            } //call function to create reminder if has_reminder is set to 1 and is_recurring is set to 0
            elseif ($create_event && $event['has_reminder'] == 1 && $event['is_recurring'] == 0) {
                $this->reminder->createReminder($event);
            } //call function to create recurring pattern if has_reminder is ser to 0 and is_recurring is set to 1

            if ($has_guests) { //if  guests exists then call function to create child tasks which creates child tasks and recurring pattern
                $event['parent_event_id'] = $event['event_id'];
                $this->createChildTasks($event, $guests);
            }

            return true;
        } catch (ModelNotFoundException $ex) {

            throw new ModelNotFoundException($ex->getMessage());
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

    }

    public function editEvent($event_id, $event)
    {
        try {
            $guests = [];
            $existing_guests = [];// store the existing guests ids based on the guests provided in input
            $new_guests = []; // stores the new guests provided in input
            $remove_guest_ids = []; // stores the guests ids which exists in database but is not provided in the input
            $existing_guests_ids = [];
            $event['existing_child'] = [];
            $event['remove_child'] = [];
            if (isset($event['guests'])) { // check if the event array contains guests index
                $search_array = array_search($event['created_by'], $event['guests']);//search in guests array to check if the organiser email exists
                if ($search_array !== false) { // if organiser mail id exists
                    unset($event['guests'][$search_array]); // remove organiser mail from the guest array
                }

                $guests = $event['guests'];
                $event['guests'] = implode(',', $guests);// convert guest array into string separated by comma (this is because guest column is of datatype string)
                count($guests) > 0 ? $has_guests = true : $has_guests = false; // if guests count is greater than 0 then multiple guest exists
            }
//dd($event_id);
            $parent_event = $this->event->getSpecificEvent($event_id, true);
            /**
             * get all the childs  id and email of parent @param $event_id
             */
            $get_childs = $this->event->fetchChildsOfParent($event_id);

            if(!(in_array('all',$parent_event->event_permission) || in_array('modify',$parent_event->event_permission))){
                throw new NotAllowedException('You do not have permission to modify this event.');
            }
            if(!in_array('all',$parent_event->event_permission)){
                $event['event_permission'] = $parent_event->event_permission;
            }

            /**
             * find the new guests and existing guest by comparing the input guests email with the existing childs email
             */
            foreach ($guests as $guest) {
                if (array_search($guest, array_column($get_childs, 'created_by')) === false) {
                    $new_guests[] = $guest;
                } else {
                    $existing_guests[] = $guest;
                }
            }

            foreach ($get_childs as $child) {
                if (array_search($child->created_by, $existing_guests) !== false) {
                    $existing_guests_ids[] = $child->id;
                } else {
                    $remove_guest_ids[] = $child->id;
                }
            }

            $event['event_id'] = $event_id;
//            return $event;

            if (!empty($existing_guests_ids) || !empty($remove_guest_ids)) {
                $event['existing_child'] = $existing_guests_ids;
                $event['remove_child'] = $remove_guest_ids;

            }
            $event['new_guests'] = $new_guests;
            $update_events = $this->updateEvent($event->all(),$get_childs);
//            $event = $event->except('guests');
//            if (!empty($new_guests)) {
//                $event['parent_event_id'] = $event_id;
//                $event['is_recurring'] = 0;
//                $event['has_reminder'] = 0;
//                unset($event['rrule'],$event['id']);
//                $this->createChildTasks($event, $new_guests); // create non recurring child event without reminder
//            }
//            array_push($event['existing_child'], $event_id);
            return true;
        }
        catch (NotAllowedException $exception){
            throw new NotAllowedException($exception->getMessage());
        } catch (ModelNotFoundException $ex) {

            throw new ModelNotFoundException($ex->getMessage());
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Fetch the child events and delete all the child events including itself
     * @param $event_id
     * @return bool|mixed
     * @throws \Exception
     */
    public function deleteEvent(array $event)
    {
        try {
            $event_from_db = $this->event->getSpecificEvent($event['event_id']);
            if($this->grant_type == 'password'){
                $user = Auth::user();
                if($event_from_db['created_by'] !== $user->email){
                    throw new NotAllowedException('You can delete your event only.');
                }
            }
            else{
                $user_id = Config::get('config.user_id');
                $user = $this->user->getSpecificUser($user_id,1);
                if($event_from_db['created_by'] !== $user->email){
                    throw new NotAllowedException('You can delete your event only.');
                }
            }
//            dd('here');
            //check if the the request event match with the event from database
            if($event_from_db['is_recurring'] == $event['is_recurring']){
                //in case of non recurring event and orig_event_id is present .simply unset orig_event_id
                if($event['is_recurring'] == 0 && (isset($event[$this->mappings['orig_event_id']]) || isset($event['redit']))){
                    if(isset($event[$this->mappings['orig_event_id']])){
                        unset($event[$this->mappings['orig_event_id']]);
                    }
                    if(isset($event['redit'])){
                        unset($event['redit']);
                    }
                }
                else{
                    //in case of recurring event if the edit mode is not present throw an error message.
                    if(!isset($event[$this->mappings['recur_edit_mode']]) ){
                        throw new NotAllowedException('Edit Mode is required in case of recurring event.');
                    }
                    if($event_from_db['rrule'] == '' && isset($event[$this->mappings['orig_event_id']])){
                        unset($event[$this->mappings['orig_event_id']]);
                    }
                    else{
                        $event['rrule'] = $event_from_db['rrule'];
                        $event['duration'] = $event_from_db['duration'];
//                        dd('here');
                        //checking if the passed event id and start date matches with the possible recurring events in future
                        if(!$this->checkRecurringEventPattern($event,$event_from_db)){
                            throw new NotAllowedException('Passed start_date and end_date does not belongs in original event occurrence.');
                        }
                    }

                }
            }
            else{
                throw new NotAllowedException();
            }
//            dd('here');
            $this->deleteRecurringEvent($event);
            return true;
        }
        catch (NotAllowedException $exception){
//            dd($exception->getMessage());
//            dd($exception->getMessage());
            throw new NotAllowedException($exception->getMessage());
        }
        catch (ModelNotFoundException $exception) {
            throw new ModelNotFoundException('Event could not be found.');
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

    }

    /**
     * Creates multiple child events based on guests added by organizer.
     * @param array $event
     * @param $guests
     * @return bool
     * @throws \Exception
     */
    private function createChildTasks(array $event, $guests)
    {
        $users = $this->user->getUserByEmails($guests);
        $user_ids = collect($users)->pluck('id')->toArray();
        $calendar_users = $this->calendar_users->createOrFetchCalendars($event['calendar_detail'],$user_ids);
        unset($event['calendar_detail']);
        foreach ($users as $guest) { //loop through guests array
            foreach ($calendar_users as $calendar_user){
                if($calendar_user['user_id'] == $guest['id']){
                    $event['user_calendar_id'] = $calendar_user['id'];
                    $event['created_by'] = $guest['email']; // assign created by to guest email
                    $event['visibility'] = 'free';
                    $event['privacy'] = 'public';
//                    dd($event);
                    $create_event = $this->addEvent($event);//create child event
                }
            }

        }
        return true;
    }

    /**
     * Loop through nested child array (which is in the tree structure of unknown depth) and return all the nodes in a single array
     * @param $child_event
     * @param int $key
     * @return array
     */
    private function processChildEvents($child_event, $key = 0)
    {
        $output = [];
        // For each array
        foreach ($child_event as $key => $event) {
            // separate its children
            $children = !empty($event['all_child_events']) ? $event['all_child_events'] : [];
            $event['all_child_events'] = [];

            // and add it to the output array

            $output[] = $event;

            // Recursively flatten the array of children

            $children = $this->processChildEvents($children, $key);

            //  and add the result to the output array
            foreach ($children as $child) {
                $output[] = $child;
            }
        }
        return $output;

    }

    /**
     * THis function will loop through each events and gets the RSVP stats like total number of awaiting,going,maybe or not going plus the list of members.
     * @param array $events
     * @param array $rsvp_record
     * @return array
     */
    private function getRsvpReport(array $events, array $rsvp_record)
    {


        foreach ($events as $event) {
            if ($event['rsvp'] == 'yes') {
                $rsvp_record['total_going'] = $rsvp_record['total_going'] + 1;
            } elseif ($event['rsvp'] == 'no') {
                $rsvp_record['total_not_going'] = $rsvp_record['total_not_going'] + 1;
            } elseif ($event['rsvp'] == 'maybe') {
                $rsvp_record['total_maybe'] = $rsvp_record['total_maybe'] + 1;
            } else {
                $rsvp_record['total_awaiting'] = $rsvp_record['total_awaiting'] + 1;
            }
            $rsvp_record['guests'][] = [
                'mail' => $event['created_by'],
                'rsvp_status' => $event['rsvp']

            ];
        }
        $rsvp_record['guests'] = array_values(array_unique($rsvp_record['guests'], SORT_REGULAR));

        return $rsvp_record;

    }


    private function generateInstances($event, $viewStartDate, $viewEndDate)
    {
        $rrule = $event['rrule'];
        $instances = array();
        $counter = 0;
        // Get any exceptions for this event. Ideally you would join exceptions
        // to events at the event query level and not have to do a separate select
        // per event like this, but for the demos this is good enough.
        $exceptions = $this->event_exception->getExceptionDates([$event[$this->mappings['id']]]);
        if (!isset($rrule) || $rrule === '') {
            array_push($instances, $event);
        } else {
            $duration = $event[$this->mappings['duration']];
            if (!isset($duration) || is_null($duration)) {
                // Duration is required to calculate the end date of each instance. You could raise
                // an error here if appropriate, but we'll just be nice and default it to 0 (i.e.
                // same start and end dates):
                $duration = 0;
            }
            // Start parsing at the later of event start or current view start:
            $rangeStart = max($viewStartDate, $event[$this->mappings['start_date']]);
            //$rangeStart = self::adjustRecurrenceRangeStart($rangeStart, $event);
            $rangeStartTime = strtotime($rangeStart);
            // Stop parsing at the earlier of event end or current view end:
            $rangeEnd = min($viewEndDate, $event[$this->mappings['r_end_date']]);
//            dd($rangeEnd);
//            dd($rangeStart);
            $rangeEndTime = strtotime($rangeEnd);
            // Third-party recurrence parser -- see recur.php
            $recurrence = new BaseEvent();
            // TODO: Using the event start date here is the correct approach, but is inefficient
            // based on the current recurrence library in use, which does not accept a starting
            // date other than the event start date. The farther in the future the view range is,
            // the more processing is required and the slower performance will be. If you can parse
            // only relative to the current view range, parsing speed is much faster, but it
            // introduces a lot more complexity to ensure that the returned dates are valid for the
            // recurrence pattern. For now we'll sacrifice performance to ensure validity, but this
            // may need to be revisited in the future.
            $rdates = $recurrence->recur($event[$this->mappings['start_date']])->rrule($rrule);

            // Counter used for generating simple unique instance ids below
            $idx = 1;
            // Loop through all valid recurrence instances as determined by the parser
            // and validate that they are within the valid view range (and not exceptions).
            while ($rdate = $rdates->next()) {
                $rtime = strtotime($rdate->format('c'));
                // When there is no end date or maximum count as part of the recurrence RRULE
                // pattern, the parser by default will simply loop until the end of time. For
                // this reason it is critical to have these boundary checks in place:
                if ($rtime < $rangeStartTime) {
                    // Instance falls before the range: skip, but keep trying

                    continue;
                }
                if ($rtime > $rangeEndTime) {
                    // Instance falls after the range: the returned set is sorted in date
                    // order, so we can now exit and return the current event set

                    break;

                }
                // Check to see if the current instance date is an exception date
                $exmatch = false;
                foreach ($exceptions as $exception) {
                    if ($exception[$this->mappings['exdate']] == $rdate->format($this->date_format)) {
                        $exmatch = true;
                        break;
                    }
                };
                if ($exmatch) {
                    // The current instance falls on an exception date so skip it
                    continue;
                }
                // Make a copy of the original event and add the needed recurrence-specific stuff:
                $copy = $event;
//                dd('here');
                // First off, set the series start date on each instance for editing purposes
                $eventStart = new \DateTime($event[$this->mappings['start_date']]);
//                dd($eventStart);
                $copy[$this->mappings['recur_series_start']] = $eventStart->format($this->date_format);
                // On the client side, Ext stores will require a unique id for all returned events.
                // The specific id format doesn't really matter ($this->mappings['orig_event_id will be used
                // to tie them together) but all ids must be unique:

                $copy[$this->mappings['id']] = $event[$this->mappings['id']] . '-rid-' . $idx++;
                // Associate the instance to its master event for later editing:
                $copy[$this->mappings['orig_event_id']] = $event[$this->mappings['id']];
                // Save the duration in case it wasn't already set:
                $copy[$this->mappings['duration']] = $duration;
                // Replace the series start date with the current instance start date:
                $copy[$this->mappings['start_date']] = $rdate->format($this->date_format);
                // By default the master event's end date will be the end date of the entire series.
                // For each instance, we actually want to calculate a proper instance end date from
                // the duration value so that the view can simply treat them as standard events:
                //keeping the recurring_end_date intact
//                dd('PT' . $duration . 'M');
                $copy['end_date'] = $rdate->add(new \DateInterval('PT' . $duration . 'M'))->format($this->date_format);
                // Add the instance to the set to be returned:
//                dd($copy);
                array_push($instances, $copy);

                if (++$counter > $this->max_event_instances) {
                    // Should never get here, but it's our safety valve against infinite looping.
                    // You'd probably want to raise an application error if this happens.
                    break;
                }
            }
        }
        return $instances;
    }


    /**
     * Returns the duration of the event in minutes
     */
    private function calculateDuration($startDate, $endDate)
    {
        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);
        return  $minutes = $start->diffInMinutes($end,false);

    }

    /**
     * If the event is recurring, this function calculates the best
     * possible end date to use for the series. It will attempt to calculate
     * an end date from the RRULE if possible, and will fall back to the PHP
     * max date otherwise. The generateInstances function will still limit
     * the results regardless. For non-recurring events, it simply returns
     * the existing end date value as-is.
     */
    private function calculateEndDate($event)
    {
        $end = isset($event[$this->mappings['r_end_date']])?$event[$this->mappings['r_end_date']]:'';
        $rrule = $event[$this->mappings['rrule']];
        $isRecurring = isset($rrule) && $rrule !== '';
        if ($isRecurring) {
            $max_date = new \DateTime('9999-12-31');
            $recurrence = new BaseEvent();
//            dd($rrule);
            $recurrence->rrule($rrule);
            if (isset($recurrence->end_date) && $recurrence->end_date < $max_date) {
                // The RRULE includes an explicit end date, so use that
                $end = $recurrence->end_date->format($this->date_format) . 'Z';
            } else if (isset($recurrence->count) && $recurrence->count > 0) {
                // The RRULE has a limit, so calculate the end date based on the instance count
                $count = 0;
                $rdates = $recurrence->recur($event[$this->mappings['start_date']]);


                while ($rdate = $rdates->next()) {
                    $newEnd = $rdate;
                    if (++$count > $recurrence->count) {
                        break;
                    }
                }
                // The 'minutes' portion should match Extensible.calendar.data.EventModel.resolution:
                $newEnd->modify('+' . $event[$this->mappings['duration']] . ' minutes');
                $end = $newEnd->format($this->date_format) . 'Z';

            } else {
//                dd('here');
                // The RRULE does not specify an end date or count, so default to max date
                $end = date($this->date_format, mktime(0, 0, 0, 12, 31, 9999)) . 'Z';
            }
        }
        return $end;
    }

    /**
     * Remove any extra attributes that are not mapped to db columns for persistence
     * otherwise MySQL will throw an error
     */
    private function cleanEvent($event)
    {
//        dd($event);
        unset($event[$this->mappings['orig_event_id']]);
        unset($event[$this->mappings['recur_edit_mode']]);
        unset($event[$this->mappings['recur_instance_start']]);
        unset($event[$this->mappings['recur_series_start']]);
        return $event;
    }

    /**
     * Return a single, non-recurring copy of an event with no id
     */
    private function createSingleCopy($event)
    {
        $copy = $event;
        $copy['is_recurring'] = 0;
        unset($copy[$this->mappings['id']]);
        unset($copy[$this->mappings['rrule']]);
        return $this->addEvent($copy);
    }

    private function addEvent($event)
    {

        $isRecurring = false;
        if (isset($event[$this->mappings['rrule']])) {
            if ($event[$this->mappings['rrule']] !== '') {
                $isRecurring = true;
            }
        }
        if ($isRecurring) {
            if(!isset($event[$this->mappings['r_end_date']] )  ){
                $event[$this->mappings['r_end_date']] = null;
            }
            // If this is a recurring event, first calculate the duration between
            // the start and end datetimes so that each recurring instance can
            // be properly calculated.
            $event['duration'] = $this->calculateDuration(
                $event[$this->mappings['start_date']], $event[$this->mappings['end_date']]);


            // Now that duration is set, we have to update the event end date to
            // match the recurrence pattern end date (or max date if the recurring
            // pattern does not end) so that the stored record will be returned for
            // any query within the range of recurrence.
//            $r_end_date =
//            dd('here');
            $r_end_date = Carbon::parse( $this->calculateEndDate($event))->format('Y-m-d H:i:s');
//dd($r_end_date);
            $event[$this->mappings['r_end_date']] = $r_end_date ;
        }

        $event = $this->cleanEvent($event);
        return $this->event->createEvent($event);
    }

    /**
     * Helper method that updates the UNTIL portion of a recurring event's RRULE
     * such that the passed end date becomes the new UNTIL value. It handles updating
     * an existing UNTIL value or adding it if needed so that there is only one
     * unqiue UNTIL value when this method returns.
     */
    private function endDateRecurringSeries($event, $endDate)
    {
        $event[$this->mappings['r_end_date']] = $endDate->format('Y-m-d H:i:s');
        $parts = explode(';', $event[$this->mappings['rrule']]);
        $newRrule = array();
//        $untilFound = false;
        foreach ($parts as $key => $part) {
//            if (strrpos($part, 'UNTIL=') === false) {
//                array_push($newRrule, $part);
//            }

            if (strrpos($part, 'UNTIL=') !== false || strrpos($part, 'COUNT=') !== false || empty($part)) {
                unset($parts[$key]);
            }
        }
        $newRrule =  $parts = array_unique($parts);
        array_push($newRrule, 'UNTIL=' . $endDate->format($this->rrule_date_format));
//        $event[$this->mappings['r_end_date']] = Carbon::parse()
//        dd($event);
        $event[$this->mappings['rrule']] = implode(';', $newRrule);
        return $event;
    }


    /**
     * Destroy the event, or possibly add an exception for a recurring instance
     */
    private function deleteRecurringEvent(array $event)
    {
        $recur_instance_start = null;
        $get_childs = [];
        $parent_event_id = null;
//        dd($event);
        if(isset($event[$this->mappings['orig_event_id']])){
            // This is a recurring event, so determine how to handle it.
            // First, load the original master event for this instance:

            $get_childs = $this->event->fetchChildsOfParent($event[$this->mappings['orig_event_id']]);
            $ids = collect($get_childs)->pluck('id')->all();
            array_push($ids,$event[$this->mappings['orig_event_id']]);
            $recur_instance_start = $event['start_date'];
            $parent_event_id = $event[$this->mappings['orig_event_id']];

        }
        else{
            $get_childs = $this->event->fetchChildsOfParent($event['id']);
            $ids = collect($get_childs)->pluck('id')->all();
            $recur_instance_start = $event['start_date'];
            array_push($ids,$event['id']);
            $parent_event_id = $event['id'];
        }

        if (isset($event[$this->mappings['recur_edit_mode']]) ) {
            $editMode = $event[$this->mappings['recur_edit_mode']];


            switch ($editMode) {
                case 'single':
                    // Not actually deleting, just adding an exception so that this
                    // date instance will no longer be returned in queries:
                    $this->event_exception->addExceptionDate($ids, new DateTime($recur_instance_start));
                    break;

                case 'future':
                    // Not actually deleting, just updating the series end date so that
                    // any future dates beyond the edited date will no longer be returned.
                    // Use this instance's start date as the new end date of the master event:
                    $endDate = new DateTime($event[$this->mappings['start_date']]);
                    // We're at the day level of precision, so roll the end date back to the
                    // end of the previous day so it will display correctly in the UI.
                    $endDate->setTime(0, 0, 0)->modify('-1 second');

                    // Now update the RRULE with this new end date also:
                    $master_event = $this->endDateRecurringSeries($event, $endDate);

                    $this->event->updateEvent($parent_event_id,[
                        'rrule' => $master_event['rrule']
                    ]);

                    foreach ($get_childs as $child){
                        $this->event->updateEvent($child->id,[
                            'rrule' => $master_event['rrule']
                        ]);
                    }

                    break;

                case 'all':
                    // Actually destroy the master event and remove any existing exceptions
                    $this->event->deleteEvent($parent_event_id);
                    $this->event_exception->removeExceptionDates($ids);
                    break;
            }
        } else {
            $this->event->deleteEvent($parent_event_id);
            $this->event_exception->removeExceptionDates($ids);
        }

        return $parent_event_id;
    }

    /**
     * Update an event or recurring series
     */
    private function updateEvent($event,$childs)
    {
        $event['event_permission'] = isset( $event['event_permission'])?implode(',',  $event['event_permission']):'read-only';
        $recur_instance_start = null;
        $has_reminder = ($event['has_reminder'] == 1)? true:false;
        $parent_event_id = $event['event_id'];
        $new_parent_id = null;
//        dd($event);
        $childs = json_decode(json_encode($childs), true);
        foreach ($childs as $key => $child){
            if(in_array($child['id' ],$event['remove_child'])){
                unset($childs[$key]);
                continue;
            }
            if(!is_array($child['event_permission']) ){
                $childs[$key]['event_permission'] = explode(',',$child['event_permission']);
            }
            unset($childs[$key]['@pv := ?']);


        }
        $master_event = $this->event->getSpecificEvent($event['event_id'],false);
        $parent_and_child_events = array_merge([$master_event->toArray()],$childs);

        if (isset( $event[$this->mappings['recur_edit_mode']]) && !empty($event['rrule'])) {
            $editMode = $event[$this->mappings['recur_edit_mode']];
            // This is a recurring event, so determine how to handle it.
            // First, load the original master event for this instance:

            $recur_instance_start = $event['start_date'];
            $event_id = isset($event['origid'])? $event['origid'] : $master_event[$this->mappings['id']];
            switch ($editMode) {

                // Both the "single" and "future" cases currently create new
                // events in order to represent edited versions of recurrence instances.

                case 'single':
                    foreach ($parent_and_child_events as $_event){
                        $new_request_event = $event;
                        if(!(in_array('all',$_event['event_permission']) || in_array('modify',$_event['event_permission']))){
                            continue;
                        }
                        if(!is_null($_event['parent_event_id'])){
                            $new_request_event['created_by'] = $_event['created_by'];
                            $new_request_event['guests'] = $_event['guests'];
                            $new_request_event['parent_event_id'] = $_event['parent_event_id'];
                            $new_request_event['has_reminder'] = $_event['has_reminder'];
                            $new_request_event['visibility'] = $_event['visibility'];
                            $new_request_event['user_calendar_id'] = $_event['user_calendar_id'];
                        }
                        else{
                            $new_request_event['event_permission'] ='all';
                        }

                        if(is_array($new_request_event['guests'])){
                            $new_request_event['guests'] = implode(',',$new_request_event['guests']);
                        }
//                        dd($new_request_event);
                        // Create a new event based on the data passed in (the
                        // original event does not need to be updated in this case):
                        $new_event_id = $this->createSingleCopy($new_request_event)->id;
//                        $new_request_event['event_permission'] = implode(',',$event['event_permission']);
                        $new_request_event['event_id'] = $new_event_id;
                        if($_event['id'] == $event['event_id']){
                            $remind =   $this->reminder->createReminder($new_request_event);
                        }
                        // Add an exception for the original occurrence start date
                        // so that the original instance will not be displayed:
//                        dd($recur_instance_start);
//                        $master_event_start_date = Carbon::parse($master_event['start_date']);
//                        $master_event_end_date = Carbon::parse($master_event['end_date']);
//                        $new_request_event_start_date = Carbon::parse($new_request_event['start_date']);
//                        $new_request_event_end_date = Carbon::parse($new_request_event['end_date']);
                        $this->event_exception->addExceptionDate([$_event['id']], new DateTime($recur_instance_start));
//                        if($master_event_end_date->ne($new_request_event_end_date) || $master_event_start_date->ne($new_request_event_start_date)){
//                            $this->event_exception->addExceptionDate([$_event['id']], new DateTime($master_event['start_date']));
//                        }
//                        else{
//                            $this->event_exception->addExceptionDate([$_event['id']], new DateTime($recur_instance_start));
//                        }
                        if(is_null($_event['parent_event_id'])){
                            $new_parent_id = $new_event_id;
                            if(!empty($new_request_event['new_guests'])){
                                $new_request_event['parent_event_id'] = $new_parent_id;
                                $new_request_event['guests'] = '';
                                $this->createChildTasks($new_request_event,$new_request_event['new_guests']);
                            }
                        }
                    }
                    break;
//

//                    $new_event_id = $this->createSingleCopy($event)->id;
//                    $event['event_id'] = $new_event_id;
//                    if($has_reminder){
//                        $remind =   $this->reminder->createReminder($event);
//                    }
//                    // Add an exception for the original occurrence start date
//                    // so that the original instance will not be displayed:
//
//                    foreach ($childs as $key => $child_event){
//                        $new_event_id = $this->createSingleCopy($child_event)->id;
//                        $childs[$key]['event_id'] = $new_event_id;
//
//
//                    }
//                    foreach ($childs as $child){
////                        dd($child);
//                        $this->reminder->createReminder($child);
////                        dd('her');
//                        // Add an exception for the original occurrence start date
//                        // so that the original instance will not be displayed:
//                        $this->event_exception->addExceptionDate([$child['id']], new DateTime($recur_instance_start));
//                    }
//                    break;

                case 'future':
                    // In this sample code we're going to split the original event
                    // into two: the original up to the edit date and a new event
                    // from the edit date to the series end date. Because of this we
                    // only end-date the original event, don't update it otherwise.
                    // This could be done all within a single event as explained in
                    // the comments above, but for this example we're keeping it simple.

                    $new_request_event = $event;
                    // First update the original event to end at the instance start:
                    $endDate = new DateTime($recur_instance_start);

                    // We're at the day level of precision, so roll the end date back to the
                    // end of the previous day so it will display correctly in the UI.
                    $endDate->setTime(0, 0, 0)->modify('-1 second');
//                    dd($endDate);
                    // Save the original end date before changing it so that we can
                    // apply it below to the newly-created series:
                    $originalEndDate = $master_event[$this->mappings['end_date']];
//                    dd($originalEndDate);
                    // End-date the master event (including the RRULE) to the instance start:
                    foreach ($parent_and_child_events as $_event){
                        if(!(in_array('all',$_event['event_permission']) || in_array('modify',$_event['event_permission']))){
                            continue;
                        }
                        $_event = $this->endDateRecurringSeries($_event, $endDate);
                        if(is_array($_event['guests'])){
                            $_event['guests'] = implode(',',$_event['guests']);
                        }


                        $this->event->updateEvent($_event['id'],$this->cleanUpdateEvent($_event,['recurring_end_date','rrule']));
                        if($_event['id'] == $event['event_id'] ){
                            if($event['has_reminder'] == 0){
                                $event['reminder'] = [];
                            }
                            $this->reminder->updateReminder($event);
                        }

                        // Now create the new event for the updated future series.
                        // Update the recurrence instance start date to the edited date:
                        $event[$this->mappings['recur_instance_start']] = $event[$this->mappings['start_date']];
//                    dd($event);
                        // Don't reuse the existing instance id since we're creating a new event:
                        unset($event[$this->mappings['id']]);
//                    dd('here');
                        // Overwrite the instance end date with the master (series) end date:
                        $event[$this->mappings['end_date']] = $originalEndDate;
                        // Create the new event (which also persists it). Note that we are NOT calling
                        // addEvent() here, which recalculates the end date for recurring events. In this
                        // case we always want to keep the existing master event end date.
                        if(!is_null($_event['parent_event_id'])){
                            $event['created_by'] = $_event['created_by'];
                            $event['guests'] = $_event['guests'];
                            $event['parent_event_id'] = $new_parent_id;
                            $event['has_reminder'] = $_event['has_reminder'];
                            $event['visibility'] = $_event['visibility'];
                            $event['user_calendar_id'] = $_event['user_calendar_id'];

                        }
                        else{
//                            dd($_event);
                            $new_request_event['event_permission'] = 'all';
                        }

//                        dd($new_request_event);
                        $event_id = $this->addEvent($new_request_event)->id;
                        $new_request_event['event_permission'] = $event['event_permission'];
                        if(is_null($_event['parent_event_id'])){
                            $new_parent_id = $event_id;
                            if(!empty($new_request_event['new_guests'])){
                                $new_request_event['parent_event_id'] = $new_parent_id;
                                $new_request_event['guests'] = '';
                                $this->createChildTasks($new_request_event,$new_request_event['new_guests']);
                            }
                        }
                    }

                    break;

                case 'all':
                    // Make sure the id is the original id, not the instance id:
                    $event[$this->mappings['id']] = $event_id;
                    // Base duration off of the current instance start / end since the end
                    // date for the series will be some future date:
                    $event[$this->mappings['duration']] = $this->calculateDuration(
                        $event[$this->mappings['start_date']], $event[$this->mappings['end_date']]);
                    // In case the start date was edited by the user, we need to update the
                    // original event to use the new start date
                    $instanceStart = new DateTime($event[$this->mappings['start_date']]);
                    $editedStart = new DateTime($event[$this->mappings['start_date']]);
                    $startDiff = $instanceStart->diff($editedStart);

                    // If start date has changed we're going to use the edited start date as the new
                    // series start date, so there's nothing else to do. However if start date is
                    // unchanged, we'll need to reset the instance start to the original series start
                    // so that we don't shift the recurring series on every edit. We'll also have to
                    // check whether the start time has changed, and if so, apply the edited offset
                    // to the original series start date. Fun!
                    if ($startDiff->days === 0) {
                        // Capture any edited time diff before we overwrite the start date
                        $startTimeDiff = $this->calculateDuration($event[$this->mappings['start_date']],
                            $event[$this->mappings['start_date']]);

                        // The start date has not changed, so revert to the
                        // original series start since we are updating the master event
                        $event[$this->mappings['start_date']] = $master_event[$this->mappings['start_date']];

                        if ($startTimeDiff !== 0) {
                            // The start time has changed, so even though the day is the same we
                            // still have to update the master event with the new start time
                            $seriesStart = new DateTime($event[$this->mappings['start_date']]);
                            if ($startTimeDiff > 0) {
                                $interval = new DateInterval('PT' . $startTimeDiff . 'M');
                            } else {
                                // Goofy logic required to handle negative diffs correctly for PHP
                                $interval = new DateInterval('PT' . (-1 * $startTimeDiff) . 'M');
                                $interval->invert = 1; // Good old PHP
                            }
                            // Apply the time offset to the original start date
                            $event[$this->mappings['start_date']] = $seriesStart->add($interval)->format($this->date_format);
                        }
                    }

                    // Finally, update the end date to the original series end date. This is
                    // especially important in the case where this series may have been split
                    // previously (e.g. by a "future" edit) so we want to preserve the current
                    // end date, and not assume that it should be the default max date.
                    $event[$this->mappings['r_end_date']] = Carbon::parse($this->calculateEndDate($event))->format('Y-m-d H:i:s');
                    // Persist changes:
//                dd($parent_and_child_events);
                    foreach ($parent_and_child_events as $key => $_event){
                        $new_request_event = $event;

                        if(!(in_array('all',$_event['event_permission']) || in_array('modify',$_event['event_permission']))){
                            continue;
                        }
//                        if($key == 0) continue;
                        if(!is_null($_event['parent_event_id'])){
                            $new_request_event['created_by'] = $_event['created_by'];
                            $new_request_event['guests'] = $_event['guests'];
                            $new_request_event['parent_event_id'] = $_event['parent_event_id'];
                            $new_request_event['has_reminder'] = $_event['has_reminder'];
                            $new_request_event['visibility'] = $_event['visibility'];
                        }
                        else{
                            $new_request_event['event_permission'] = 'all';
                        }
                        if(is_array($new_request_event['guests'])){
                            $new_request_event['guests'] = implode(',',$new_request_event['guests']);
                        }
                        $this->event->updateEvent($_event['id'],$this->cleanUpdateEvent($new_request_event));
                        if($_event['id'] == $event['event_id'] ){
                            if($event['has_reminder'] == 0){
                                $event['reminder'] = [];
                            }
                            $this->reminder->updateReminder($event);
                        }
                    }
                    $event['parent_event_id'] = $event['id'];
                    $event['guests'] = '';
                    $this->createChildTasks($event,$event['new_guests']);

                    break;
            }
        } else {
            if (isset($event[$this->mappings['rrule']])  && $event[$this->mappings['rrule']]) {
                // There was no recurrence edit mode, but there is an rrule, so this was
                // an existing non-recurring event that had recurrence added to it. Need
                // to calculate the duration and end date for the series.
                $event[$this->mappings['duration']] = $this->calculateDuration(
                    $event[$this->mappings['start_date']], $event[$this->mappings['end_date']]);
                $event[$this->mappings['r_end_date']] = Carbon::parse($this->calculateEndDate($event))->format('Y-m-d H:i:s');
            } else if (isset($event[$this->mappings['orig_event_id']]) && $event[$this->mappings['orig_event_id']]) {
                // In case the original event was recurring and was made non-recurring
                // we need to reset the original id and clean it up
                $event[$this->mappings['id']] = $event[$this->mappings['orig_event_id']];
                // Null out the recurrence-specific fields that are left over
                $event[$this->mappings['rrule']] = '';
                $event[$this->mappings['duration']] = null;
//                unset($event[$this->mappings['rrule']]);
//                unset($event[$this->mappings['duration']]);
            }
            foreach ($parent_and_child_events as $_event){
                $new_request_event = $event;
                if(!(in_array('all',$_event['event_permission']) || in_array('modify',$_event['event_permission']))){
                    continue;
                }
                if(!is_null($_event['parent_event_id'])){
                    $new_request_event['created_by'] = $_event['created_by'];
                    $new_request_event['guests'] = $_event['guests'];
                    $new_request_event['parent_event_id'] = $_event['parent_event_id'];
                    $new_request_event['has_reminder'] = $_event['has_reminder'];
                    $new_request_event['visibility'] = $_event['visibility'];
                }
                else{
                    $new_request_event['event_permission'] = 'all';
                }
                $new_request_event = $this->cleanUpdateEvent($new_request_event);
//                dd($_event);
                $this->event->updateEvent($_event['id'],$new_request_event);
                if($_event['id'] == $event['event_id'] ){
                    if($event['has_reminder'] == 0){
                        $event['reminder'] = [];
                    }
                    $this->reminder->updateReminder($event);
                }
            }
            $event['parent_event_id'] = $event['id'];
            $event['guests'] = '';
            $this->createChildTasks($event,$event['new_guests']);

        }

        return $event;
    }
    //this will check if input event occurrence matches or not
    private function checkRecurringEventPattern($event,$original_event){
        $rrule = $event[$this->mappings['rrule']];
//        $instances = array();
        $counter = 0;
        // Get any exceptions for this event. Ideally you would join exceptions
        // to events at the event query level and not have to do a separate select
        // per event like this, but for the demos this is good enough.
        $exceptions = $this->event_exception->getExceptionDates([$event[$this->mappings['id']]]);
//dd($exceptions);
//choose the min start date to generate the exact pattern
        $rangeStart = min($original_event[$this->mappings['start_date']],$event['start_date']);
        $rangeStartTime = strtotime($rangeStart);
        //choose the max end date
        $rangeEnd =  max($event['end_date'],$original_event['end_date']);
        $rangeEndTime = strtotime($rangeEnd);
        $recurrence = new BaseEvent();
        $duration = is_null($event[$this->mappings['duration']])?0:$event[$this->mappings['duration']];
        $rdates = $recurrence->recur($original_event[$this->mappings['start_date']])->rrule($rrule);
        // Counter used for generating simple unique instance ids below
        $idx = 1;
        // Loop through all valid recurrence instances as determined by the parser
        // and validate that they are within the valid view range (and not exceptions).
        while ($rdate = $rdates->next()) {
            $rtime = strtotime($rdate->format('c'));
            $id = 0;
            // When there is no end date or maximum count as part of the recurrence RRULE
            // pattern, the parser by default will simply loop until the end of time. For
            // this reason it is critical to have these boundary checks in place:
            if ($rtime < $rangeStartTime) {
                // Instance falls before the range: skip, but keep trying
                continue;

            }
            if ($rtime > $rangeEndTime) {
                // Instance falls after the range: the returned set is sorted in date
                // order, so we can now exit and return the current event set
                break;
            }
            // Check to see if the current instance date is an exception date
            $exmatch = false;
            foreach ($exceptions as $exception) {
                if ($exception[$this->mappings['exdate']] == $rdate->format($this->date_format)) {
                    $exmatch = true;
                    break;
                }
            };
            if ($exmatch) {
                // The current instance falls on an exception date so skip it
                continue;
            }
            $id = $event['event_id'] . '-rid-' . $idx++;
            $end_date = $rdate->add(new \DateInterval('PT' . $duration . 'M'))->format($this->date_format);

            if($id == $event['id'] || $end_date == $event['end_date']) {
                return true;
            }


            if (++$counter > $this->max_event_instances) {
                return false;
            }
        }
        return false;

    }

    private function cleanUpdateEvent($event,$filter = null){
        $filtered_event = [];
//        dd($event);
        if(is_null($filter) || empty($filter)){
            $filter =  [
                'parent_event_id','title',
                'description','start_date','end_date',
                'duration','rrule','is_full_day_event',
                'is_recurring','created_by','has_reminder',
                'location','guests','rsvp',
                'note','event_permission','visibility',
                'privacy','status','recurring_end_date','user_calendar_id','existing_child','remove_child'
            ];
        }
        foreach ($event as $key =>$_event){
            if(in_array($key,$filter)){
                $filtered_event[$key] = $_event;
            }
        }
        return $filtered_event;
    }


    /**
     * Creates multiple child events based on guests added by organizer.
     * @param array $event
     * @param $guests
     * @return bool
     * @throws \Exception
     */
    public function createChildTasksForSharing(array $event, $guests,$calendar)
    {
        $users = $this->user->getUserByEmails($guests);
        $user_ids = collect($users)->pluck('id')->toArray();
        $calendar_users = $this->calendar_users->createOrFetchCalendars($calendar,$user_ids);

//        $calendar_users = CalendarUsers::whereIn('user_id',$user_ids)->where('name','Events')->get();
        foreach ($users as $guest) { //loop through guests array
            foreach ($calendar_users as $calendar_user){
                if($calendar_user['user_id'] == $guest['id']){
                    $event['user_calendar_id'] = $calendar_user['id'];
                    $event['created_by'] = $guest['email']; // assign created by to guest email
                    $event['visibility'] = 'free';
                    $event['privacy'] = 'public';

                    $create_event = $this->addEvent($event);//create child event
                }
            }

        }
        return true;
    }





}