<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/19/19
 * Time: 10:28 AM
 */

namespace App\CalendarClass\CalendarInterface;


interface EventClassInterface
{
    /**
     * Fetch specific event details along with its reminders and recurring patterns if available
     * @param $event_id
     * @return mixed
     */
    public function getSpecificEvent($event_id);

    /**
     * fetch specific user events based on the start and end date
     * @param $user_email
     * @param null $start_date
     * @param null $end_date
     * @return mixed
     */
    public function getSpecificUserEvents($user_email,$start_date ,$end_date);

    /**
     * create an event along with reminders and recurring patterns if passed
     * @param array $event
     * @return mixed
     */
    public function createEvent(array $event);

    /**
     * Modify an event along with reminders and recurring patterns if passed
     * @param $event_id
     * @param array $event
     * @return mixed
     */
    public function editEvent($event_id,  $event);

    /**
     * Delete an events and all of its reminders and patterns
     * @param $event_id
     * @return mixed
     */
    public function deleteEvent(array $event);
}