<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/31/19
 * Time: 3:32 PM
 */

namespace App\CalendarClass\CalendarInterface;


use Illuminate\Http\Request;

interface BaseCalendarInterface
{
        public function showCalendar($email, array $calendars, $request);

}