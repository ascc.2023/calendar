<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 10:42 AM
 */

namespace App\CalendarClass\CalendarInterface;


interface ReminderInterface
{
    public function getRawReminders($event_id);

    public function getReminders($event_id);

    public function createReminder(array $event_data);

    public function deleteReminder($event_id, $reminder_id);





}