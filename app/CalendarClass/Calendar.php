<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/31/19
 * Time: 3:48 PM
 */

namespace App\CalendarClass;


use App\CalendarClass\CalendarInterface\BaseCalendarInterface;
use App\CalendarClass\CalendarInterface\CalendarInterface;
use App\CalendarClass\CalendarInterface\EventClassInterface;
use App\Config\CalendarConfig;
use App\Config\Formatters\ArrayFormatter;
use App\Models\CalendarUsers;
use App\Models\User;
use App\Repo\CalendarUserInterface;
use App\Repo\EventModelInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Calendar implements BaseCalendarInterface
{

    /** @var CalendarConfig */
    public $config;
    public $day_callback = null;
    public $month_callback = null;
    public $week_callback = null;
    public $quarter_callback = null;
    public $year_callback = null;
    private $event;
    private $current_date;
    private $grant_type;
    private $calendar_user;
    public function __construct(EventClass $event,CalendarUserInterface $calendarUser)
    {
        $carbon = new Carbon();
        $default_config = new CalendarConfig();
        $default_config->setFormatter(new ArrayFormatter());
        $this->current_date = Carbon::now('Asia/Kathmandu');
        $start_date = $this->current_date->copy()->startOfYear();
        $end_date =  $this->current_date->copy()->endOfYear();
//        $start_date->sub(new \DateInterval('P'.($start_date->format('j')-1).'D'));
        $default_config->setStartDate($start_date);
        $default_config->setEndDate($end_date);
//        $default_config->setInterval(new \DateInterval('P6M'));
        $this->event = $event;
        $this->config = $default_config;
        $this->grant_type = Config::get('config.grant_type','');
        $this->calendar_user = $calendarUser;
    }

//    public function setEvent(EventClass $class){
//        $this->event = $class;
//    }

    public function setConfig(CalendarConfig $conf)
    {
        $this->config = $conf;
    }
    public function getConfig()
    {
        return $this->config;
    }
    public function setDataInElement($date, $element)
    {
        $result = null;

        if(isset($this->$element)){
            $result = $this->$element($date);
        }
        return $result;
    }

    public function showCalendar($email,array $calendars, $request)
    {
        $calendar_ids = collect($calendars)->pluck('id')->toArray();
        $is_current_day = false;
        $period = new \DatePeriod(
           $request->start_date,
            new \DateInterval('P1D'),
            $request->end_date
        );
//dd($period);
        $data = array();
        $month_stats = [];
//        dd($calendar_ids);
        $all_events =  $this->event->getSpecificUserEvents($email, $request->start_date, $request->end_date,$calendar_ids);
//         dd($all_events);
        foreach($period as $key => $date){
            $events = [];
            $year = (int)$date->format('Y');
            $month = (int)$date->format('n');
            $day = (int) $date->format('j');
            $quarter = (int) ceil($month / 3);
            $week = (int) $date->format('W');
            $week_day = (int) $date->format('w');
            if($month == 1 && $week > 50){
                $week = 0;
            }

            $formatted_date = $date->copy()->format('Y-m-d');
//            dd($formatted_date);
            foreach ($all_events as $key1 => $event){
                if(Carbon::parse($event['start_date'])->format('Y-m-d') == $formatted_date ){
                    $events[] = $event;
                    unset($all_events[$key1]);
                }

            }
            if($day === 1){
                $month_stats['total_days'] = (int)$date->format('t');
                $month_stats['start_week_of_month '] = $week;
                $month_stats['start_week_day'] = $week_day;
            }
            if(!empty($month_stats)){
//                $data[$year][$month] = $month_stats;
                $data[$year][$month][] = [
                    'year' => $year,
                    'quarter' => $quarter,
                    'month' => $month,
                    'week' => $week,
                    'week_day' => $week_day,
                    'is_today' => (!$is_current_day && $this->current_date->copy()->format('Y-m-d') == $date->copy()->format('Y-m-d'))?true:false,
                    'day' => $day,
                    'events' => $events
                ];
            }

//            return $date;
//            if(!array_key_exists($year, $cal)){
//                $cal[$year] = array(
//                    'type' => 'year',
//                    'value' => $year,
////                    'data' => $this->setDataInElement($date, 'year_callback'),
//                    'elements' => array()
//                );
//            }
//
//
//            if(!array_key_exists($quarter, $cal[$year]['elements'])){
//                $cal[$year]['elements'][$quarter] = array(
//                    'type' => 'quarter',
//                    'value' => $quarter,
////                    'data' => $this->setDataInElement($date, 'quarter_callback'),
//                    'elements' => array()
//                );
//            }
////            return $period;
//            if(!array_key_exists($month, $cal[$year]['elements'][$quarter]['elements'])){
//                $cal[$year]['elements'][$quarter]['elements'][$month] = array(
//                    'type' => 'month',
//                    'value' => $month,
////                    'data' => $this->setDataInElement($date, 'month_callback'),
//                    'elements' => array()
//                );
//            }
//            if(!array_key_exists($week, $cal[$year]['elements'][$quarter]['elements'][$month]['elements'])){
//                $cal[$year]['elements'][$quarter]['elements'][$month]['elements'][$week] = array(
//                    'type' => 'week',
//                    'value' => $week,
////                    'data' => $this->setDataInElement($date, 'week_callback'),
//                    'elements' => array()
//                );
//            }
//            if(!array_key_exists($day, $cal[$year]['elements'][$quarter]['elements'][$month]['elements'][$week]['elements'])){
//                $cal[$year]['elements'][$quarter]['elements'][$month]['elements'][$week]['elements'][$day] = array(
//                    'type' => 'day',
//                    'value' => $day,
////                    'data' => $this->setDataInElement($date, 'day_callback'),
//                    'weekday' => $week_day,
//                    'events' => $events
//                );
//            }
        }
        return $this->config->getFormatter()->setFormat($data);
    }


}