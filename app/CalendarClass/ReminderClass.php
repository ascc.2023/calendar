<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 10:47 AM
 */

namespace App\CalendarClass;


use App\CalendarClass\CalendarInterface\ReminderInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReminderClass implements ReminderInterface
{
    private $reminder;
    public function __construct(\App\Repo\ReminderInterface $reminder)
    {
        $this->reminder = $reminder;
    }

    /**
     * Returns specific event reminders without formatting
     * @param $event_id
     * @return mixed
     */
    public function getRawReminders($event_id)
    {
        return $this->reminder->getAllEventReminders($event_id);
    }

    /**
     * Returns specific reminder by modifying the query results
     * @param $event_id
     * @return mixed
     */
    public function getReminders($event_id)
    {
        $reminders = $this->reminder->getAllEventReminders($event_id);
        foreach ($reminders as $reminder){
            if(!is_null($reminder->time))
                $reminder->time = date("h:i A",strtotime($reminder->time));
        }
        return $reminders;
    }

    /**
     * This function creates reminder based on the event data
     * @param $event_data
     * @param array $reminder_data
     * @return bool
     */
    public function createReminder(array $event_data)
    {

        try{
            $reminder_time = null;
            if(!$event_data['has_reminder']) return true;
//        $reminder_data['event_id'] = $event_data['id'];
            foreach ($event_data['reminder'] as $reminder){
//                dd($reminder);
//                dd()
                $reminder['event_id'] = $event_data['event_id'];
                if(!$event_data['is_full_day_event']){
                    $reminder['time'] = null;
                }
                if(!isset($reminder['note'])){
                    $reminder['note'] = '';
                }
                $this->reminder->createReminder($reminder);
            }
            return  true;

        }
        catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }


    }

    /**
     * Updates reminder(s)
     * @param array $reminder_data
     * @return bool
     * @throws \Exception
     */
    public function updateReminder(array $reminder_data){
        try{
            $reminder_time = null;
            $total_reminder = $this->reminder->getAllEventReminders($reminder_data['event_id']);
            $reminder_ids = collect($total_reminder)->pluck('id')->toArray();
            $request_reminder_ids = collect($reminder_data['reminder'])->pluck('id')->toArray();
            $to_be_deleted_reminder = array_diff($reminder_ids,$request_reminder_ids);
//            dd($to_be_deleted_reminder);
            $this->reminder->deleteRemindersByIds($to_be_deleted_reminder);

            if(!$reminder_data['has_reminder']) return true;
            foreach ($reminder_data['reminder'] as $reminder){
                $reminder['event_id'] = $reminder_data['event_id'];
                if(!$reminder_data['is_full_day_event']){
                    $reminder['time'] = null;
                }
                if(!isset($reminder['id'])){
                    $this->reminder->createReminder($reminder);
                }
                else{
                    $this->reminder->updateReminderByReminderAndEventId($reminder['id'],$reminder['event_id'],$reminder);
                }

            }
            return true;
        }
        catch (ModelNotFoundException $exception){
            throw new ModelNotFoundException('Reminder could not be found.');
        }
        catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }

    }

    /**
     * Delete reminder
     * @param $event_id
     * @param null $reminder_id
     * @return bool
     * @throws \Exception
     */
    public function deleteReminder($event_id, $reminder_id = null){
        try{
            if(is_null($event_id)){
                $this->reminder->deletedReminder($reminder_id);
            }
            else{
                $this->reminder->deleteRemindersByEventId($event_id);
            }
            return true;
        }
        catch (ModelNotFoundException $exception){
            throw new ModelNotFoundException('Reminder could not be found.');
        }
        catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }

    }


}