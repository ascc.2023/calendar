<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/19
 * Time: 1:17 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Calendars extends Model
{
    protected $table = 'calendars';

    protected $fillable = ['name'];

    public function users(){
        return $this->belongsToMany(User::class)->withPivot(
            'status','color','name','created_at','updated_at'
        );
    }

    public function getNameAttribute($value){
        return ucwords(strtolower($value));
    }
}