<?php

namespace App\Models;

use App\Traits\PassportToken;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens,Authenticatable, Authorizable;
    use PassportToken;
    protected $table = 'users';
    protected $attributes = [
        'social_login' => ''
    ];
//    public $hide_pivot = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email','password','gender','mobile','birthday','address','status','avatar','social_login',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','social_login','remember_token'
    ];


    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = app('hash')->make($password);
    }

//    //this will support passing username or email in the same field
//    public function findForPassport($identifier) {
//        return $this->orWhere('email', $identifier)->first();
//    }

    public function calendars(){
        return $this->belongsToMany(Calendars::class, 'calendar_users', 'user_id', 'calendar_id')->withPivot(
            'status','color','name','created_at','updated_at'
            );
    }

    public function userGroups(){
        return $this->belongsToMany(UserGroups::class, 'user_groups_pivot', 'user_id', 'user_group_id');
    }
    public function roles(){
        return $this->belongsToMany(Roles::class, 'role_users', 'user_id', 'role_id');
    }




}
