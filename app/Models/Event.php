<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 1:13 PM
 */

namespace App\Models;


//use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Recurr\Rule;
use Recurr\Transformer\TextTransformer;

class Event extends Model
{
    protected $table = 'events';
//    private $formatted_rules = [];
    protected $appends = array('formatted_rrule');
    protected $attributes = [
        'rrule' => '',
    ];
    protected $fillable = [
        'parent_event_id','title',
        'description','start_date','end_date',
        'duration','rrule','is_full_day_event',
        'is_recurring','created_by','has_reminder',
        'location','guests','rsvp',
        'note','event_permission','visibility',
        'privacy','status','recurring_end_date','user_calendar_id'
    ];

    public function parentEvent(){
        return $this->belongsToOne(static::class, 'parent_event_id');
    }

    public function childEvents(){
        return $this->hasMany(static::class, 'parent_event_id');
    }

    public function AllChildEvents(){
        return $this->childEvents()->with('AllChildEvents');
    }

    public function reminder(){
        return $this->hasMany(Reminder::class);
    }

    public function recurringPattern(){
        return $this->hasMany(RecurringPattern::class);
    }

    public function eventInstanceException(){
        return $this->hasMany(EventInstanceException::class);
    }
    public function getEventPermissionAttribute($value){
        if(!empty($value)){
            return explode(',',$value);
        }
        return $value;
    }

    public function setRruleAttribute($value){
        if(empty($value) || is_null($value)){
            $this->attributes['rrule'] = '';
        }
        else{
            $this->attributes['rrule'] = $value;
        }

    }
    public function setNoteAttribute($value){
        if(empty($value) || !isset($value)){
            $this->attributes['note'] = '';
        }
        else{
            $this->attributes['note'] = $value;
        }

    }

    public function setLocationAttribute($value){
//        dd('here');
        if(!isset($value) || is_null($value)){
            $this->attributes['location'] = '';
        }
        else{
            $this->attributes['location'] = $value;
        }

    }

    public function getGuestsAttribute($value){
        if(empty($value)) return [];
        else{
            return explode(',',$value);
        }
    }

    public function getFormattedRruleAttribute(){
        if(empty($this->attributes['rrule']) || is_null($this->attributes['rrule'])) return [];
//        dd($this->attributes['rrule']);
        $rule = new Rule($this->attributes['rrule'], null,null,null);
//      return  $rule->getWeekStart();
        $textTransformer = new TextTransformer();
//        dd($rule);
//        dd($rule->getStartDate());
        $data['human_readable'] = $textTransformer->transform($rule);
        $data['recurring_type'] = strtolower($rule->getFreqAsText());
        if($data['recurring_type'] == 'yearly'){
            $data['recurring_by_month'] = implode(',',$rule->getByMonth());
            if(!is_null($rule->getByMonthDay())){
                $data['recurring_by_month_day'] = implode(',',$rule->getByMonthDay());
            }
            if(!is_null($rule->getByDay())){
                $data['recurring_by_day'] = $this->getDaysInNumeric($rule->getByDay());
            }
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }
            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
        }
        elseif ($data['recurring_type'] == 'monthly'){
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }

            if(!is_null($rule->getByMonthDay())){
                $data['recurring_by_month_day'] = implode(',',$rule->getByMonthDay());
            }

            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
            if(!is_null($rule->getByDay())){
                $data['recurring_by_day'] = $this->getDaysInNumeric($rule->getByDay());
            }
            if(!is_null($rule->getBySetPosition())){
                $data['recurring_by_set_pos'] = $rule->getBySetPosition();
            }
        }
        elseif ( $data['recurring_type'] == 'weekly'){
            $data['recurring_by_day'] = $this->getDaysInNumeric($rule->getByDay());
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }
        }
        else{
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }
        }
        return $data;
    }

    //convert week days into numeric like 0 for SU,1 for MO
    private function getDaysInNumeric(array $week_days){
        $days = [
            'SU',"MO","TU","WE","TH","FR","SA"
        ];
        $new_numeric_day = [];
        foreach ($week_days as $day){
            $new_numeric_day[] = array_search($day,$days);
        }
        return $new_numeric_day;
    }

}