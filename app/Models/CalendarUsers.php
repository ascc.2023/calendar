<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/19
 * Time: 1:26 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CalendarUsers extends Model
{
    protected $table = 'calendar_users';
    protected $attributes = [
        'is_modifiable' => 1
    ];
    protected $fillable = ['user_id','calendar_id','status','color','created_at','updated_at','name','is_modifiable'];


}