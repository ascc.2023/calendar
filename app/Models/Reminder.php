<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 1:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $table = 'reminders';
    protected $attributes = [
      'note' => ''
    ];
    protected $fillable = [
        'event_id', 'format_value',
        'format', 'time','note',
        'notification_type'
    ];



    public function event(){
        return $this->belongsTo(Event::class,'event_id');
    }

}