<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 1:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EventPermission extends Model
{
    protected $table = 'event_permissions';

    protected $attributes = [
        'display_name' => '',
    ];
    protected $fillable = [
      'id','name','display_name'
    ];
}