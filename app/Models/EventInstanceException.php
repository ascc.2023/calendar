<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 3/17/19
 * Time: 1:17 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EventInstanceException extends Model
{
    protected $table = 'event_instance_exceptions';

    protected $fillable = [
        'event_id','ex_date'
    ];

    public function event(){
        return $this->belongsTo(Event::class,'event_id');
    }
}