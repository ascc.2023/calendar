<?php
    /**
     * Created by PhpStorm.
     * User: ashish
     * Date: 5/2/19
     * Time: 12:44 PM
     */

    namespace App\Models;


    use Illuminate\Database\Eloquent\Model;

    class UserGroups extends Model
    {
        protected $table = 'user_groups';
        protected $fillable = [
            'name','status'
        ];

        public function users(){
            return $this->belongsToMany(User::class,'user_groups_pivot', 'user_group_id', 'user_id');
        }
    }