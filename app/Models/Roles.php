<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/19
 * Time: 5:33 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'name',
        'display_name'
    ];

    public function roles(){
        return $this->belongsToMany(User::class);
    }
}