<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/19
 * Time: 5:34 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RoleUsers extends Model
{
    protected $table = 'role_users';
    protected $fillable = ['user_id','role_id'];

}