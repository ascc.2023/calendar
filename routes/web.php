<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function (){
//    set_time_limit(0);
//    $date = \Carbon\Carbon::parse('1944-01-01');
//    $date_bs = [];
//    $count = 0;
//    $converter = new \App\CalendarClass\AdBsConverter();
//    while($date->year != 2033){
//        $date_bs[$count] = $converter->eng_to_nep($date);
//        $count++;
//        $date = $date->addDay();
//    }
////    if($date->year != 2033){
////
////    }
//
//    return $date_bs;

    return app()->version();
});


$router->group(['prefix' => 'admin','middleware' => ['auth','scopes:admin']], function () use ($router) {
    $router->get('permissions', [
        "uses" => "EventPermissionController@listPermission"
    ]);

    $router->get('users',[
       'uses' => 'UserController@listAllUser'
    ]);

    $router->post('permissions',[
        "uses" => "EventPermissionController@createPermission"
    ]);

    $router->put('permissions/{id:[0-9]+}',[
        "uses" => "EventPermissionController@updateEventPermission"
    ]);

    $router->delete('permissions/{id:[0-9]+}',[
        "uses" => "EventPermissionController@deleteEventPermission"
    ]);

    $router->get('groups/{id}',[
        "uses" => "UserGroupsController@getGroups"
    ]);

    $router->get('groups/{id}',[
        "uses" => "UserGroupsController@getGroups"
    ]);

    $router->get('groups',[
        "uses" => "UserGroupsController@listGroups"
    ]);

    $router->post('groups',[
        "uses" => "UserGroupsController@createGroup"
    ]);

    $router->put('groups/user',[
        "uses" => "UserGroupsController@assignUserGroups"
    ]);

    $router->put('group/users',[
        "uses" => "UserGroupsController@assignGroupUsers"
    ]);

    $router->put('groups/{id:[0-9]+}',[
        "uses" => "UserGroupsController@updateGroup"
    ]);

    $router->delete('groups/{id:[0-9]+}',[
        "uses" => "UserGroupsController@deleteGroup"
    ]);
});




$router->group(['login and user creation group'], function () use ($router) {
    $router->post("users", [
        "uses" => "UserController@createUser"
    ]);

    $router->post('users/login',
        'AuthController@login'
    );

    $router->post('apps','AuthController@createClient');

    $router->post('login/{provider}','AuthController@loginWithProvider');

    $router->get('apps','CalendarController@getAppCalendar');
});

$router->group([ 'middleware' => ['auth','scopes:customer'] ], function () use ($router) {

    $router->get('event/permissions', [
        "uses" => "EventPermissionController@listPermissionsForPublic"
    ]);

    $router->get('calendars', [
        "uses" => "CalendarController@getUserCalendar"
    ]);

    $router->post('share/calendars', [
        "uses" => "CalendarController@shareCalendar"
    ]);

    $router->get('groups',[
        "uses" => "UserController@userGroup"
    ]);

    $router->put('calendars/{id:[0-9]+}','CalendarController@updateCalendar');

    $router->delete('calendars/{id:[0-9]+}','CalendarController@deleteCalendar');

    $router->get("reminders/{event_id:[0-9]+}", [
        "uses" => "ReminderController@getEventReminder"
    ]);

    $router->post('calendars','CalendarController@createCalendar');

    $router->get("events/{event_id:[0-9]+}", [
        "uses" => "EventsController@getEvent"
    ]);

    $router->delete("events/{id}", [
        "uses" => "EventsController@deleteEvent"
    ]);

    $router->get('events', [
        'uses' => "EventsController@getUserEvents",

    ]);

    $router->post("events", [
        "uses" => "EventsController@createEvent"
    ]);

    $router->put("events/{event_id}", [
        "uses" => "EventsController@updateEvent"
    ]);

    $router->get('users/{id:[0-9]+}', [
        'uses' => "UserController@getUserDetail",
    ]);

    $router->put('users/{id:[0-9]+}', [
        'uses' => "UserController@updateUser",
    ]);

    $router->delete('users/{id:[0-9]+}', [
        'uses' => "UserController@deleteUser",
    ]);
});


$router->group(['prefix' => 'app','middleware' => 'client:customer'], function () use ($router) {
    $router->get('calendars', [
        "uses" => "CalendarController@getUserCalendar"
    ]);

    $router->get('event/permissions', [
        "uses" => "EventPermissionController@listPermissionsForPublic"
    ]);

    $router->get('groups',[
        "uses" => "UserController@userGroup"
    ]);

    $router->post('share/calendars', [
        "uses" => "CalendarController@shareCalendar"
    ]);

    $router->put('calendars/{id:[0-9]+}','CalendarController@updateCalendar');

    $router->delete('calendars/{id:[0-9]+}','CalendarController@deleteCalendar');

    $router->post('calendars','CalendarController@createCalendar');

    $router->get("reminders/{event_id:[0-9]+}", [
        "uses" => "ReminderController@getEventReminder"
    ]);

    $router->get("events/{event_id:[0-9]+}", [
        "uses" => "EventsController@getEvent"
    ]);

    $router->delete("events/{id}", [
        "uses" => "EventsController@deleteEvent"
    ]);



    $router->get('events', [
        'uses' => "EventsController@getUserEvents",
    ]);

    $router->post("events", [
        "uses" => "EventsController@createEvent"
    ]);

    $router->put("events/{event_id}", [
        "uses" => "EventsController@updateEvent"
    ]);

    $router->get('users/{id:[0-9]+}', [
        'uses' => "UserController@getUserDetail",
    ]);

    $router->put('users/{id:[0-9]+}', [
        'uses' => "UserController@updateUser",
    ]);

    $router->delete('users/{id:[0-9]+}', [
        'uses' => "UserController@deleteUser",
    ]);

});
